#version 430

// The workgroup size must match the program's size.
layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;

// Input image data
layout(binding = 0, std430) buffer input_buf
{
   uvec4 pixels[];
} inData;

// Output image data
layout(binding = 1, std430) buffer output_buf
{
   uvec4 pixels[];
} outData;

// Push constants block
layout(push_constant) uniform constants
{
   uvec4 image_props;     // x=width, y=height, z=number_of_components, w=[unused]
} pushConsts;

const vec3 yellow_pixel = vec3(255., 255., 0);

uvec4 ConvertToLuminanceVec4(uvec4 pixel)
{
   uint luminance = uint(float(pixel.r) * 0.299 + float(pixel.g) * 0.587 + float(pixel.b) * 0.114);
   return uvec4(uvec3(luminance), 255);
}

uvec4 GenEdgeResults(mat3 x_results, mat3 y_results, float clamp)
{
   float result_x = abs(x_results[0][0] + x_results[0][1] + x_results[0][2] +
                        x_results[1][0] + x_results[1][1] + x_results[1][2] +
                        x_results[2][0] + x_results[2][1] + x_results[2][2]);
   float result_y = abs(y_results[0][0] + y_results[0][1] + y_results[0][2] +
                        y_results[1][0] + y_results[1][1] + y_results[1][2] +
                        y_results[2][0] + y_results[2][1] + y_results[2][2]);
   float percent;
   if (result_x > result_y)
   {
      percent = (result_x - clamp) / 255;
   }
   else
   {
      percent = (result_y - clamp) / 255;
   }
   if (percent < 0) percent = 0;
   return uvec4((yellow_pixel * percent), 255);
}

uvec4 RunPrewitt(mat3 neighbors)
{
   // Prewitt Matrices (GLSL takes inputs in as columns):
   //
   //             | +1   0   -1 |              | +1  +1  +1 |
   // prewitt_x = | +1   0   -1 |  prewitt_y = |  0   0   0 |
   //             | +1   0   -1 |              | -1  -1  -1 |
   //
   mat3 prewitt_x = { { 1.,  1.,  1.}, { 0.,  0.,  0.}, {-1., -1., -1.}};
   mat3 prewitt_y = { { 1.,  0., -1.}, { 1.,  0., -1.}, { 1.,  0., -1.}};

   mat3 result_mat_x = prewitt_x * neighbors;
   mat3 result_mat_y = prewitt_y * neighbors;
   return GenEdgeResults(result_mat_x, result_mat_y, 127);
}

void main()
{
   // Only use if we are within the pixel boundaries of the incoming image
   if (gl_GlobalInvocationID.x >= pushConsts.image_props.x ||
      gl_GlobalInvocationID.y >= pushConsts.image_props.y)
   {
      return;
   }

   mat3 neighbors;
   uint pixel_index;
   for (int x_mod = -1; x_mod < 2; ++x_mod)
   {
      for (int y_mod = -1; y_mod < 2; ++y_mod)
      {
         int x_index = int(gl_GlobalInvocationID.x) + x_mod;
         int y_index = int(gl_GlobalInvocationID.y) + y_mod;

         // If pixel falls outside edge of image, just duplicate the edge pixel
         if (x_index < 0)
         {
             x_index = 0;
         }
         else if (x_index > pushConsts.image_props.x - 1)
         {
             x_index = int(pushConsts.image_props.x) - 1;
         }
         if (y_index < 0)
         {
             y_index = 0;
         }
         else if (y_index > pushConsts.image_props.y - 1)
         {
             y_index = int(pushConsts.image_props.y) - 1;
         }

         pixel_index = ((y_index) * pushConsts.image_props.x) + (x_index);
         neighbors[x_mod + 1][y_mod + 1] =
            float(ConvertToLuminanceVec4(inData.pixels[pixel_index]).r);
      }
   }

   outData.pixels[pixel_index] = RunPrewitt(neighbors);
}
