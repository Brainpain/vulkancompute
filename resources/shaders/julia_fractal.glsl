#version 430

// The workgroup size must match the program's size.
layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;

// Output image data
layout(binding = 0, std430) buffer out_buf
{
   uvec4 pixels[];
}
output_image;

// Push constants block
layout(push_constant) uniform constants
{
   uvec2 image_dimensions;
   uint  max_iterations;
   float time;
}
push_constants;

#define PI 3.1415926

uvec3
julia(vec2 uv)
{
   vec2 uv_coord = uv * abs(sin(push_constants.time * 0.2));

   int j = 0;
   for (int i = 0; i < push_constants.max_iterations; i++)
   {
      j++;
      vec2 c = vec2(-0.345, 0.654);
      vec2 d = vec2(push_constants.time * 0.005, 0.0);
      uv_coord =
        vec2(uv_coord.x * uv_coord.x - uv_coord.y * uv_coord.y, 2.0 * uv_coord.x * uv_coord.y) + c +
        d;
      if (length(uv_coord) > float(push_constants.max_iterations))
      {
         break;
      }
   }
   float mod = float(j) / float(push_constants.max_iterations);
   return uvec3(mod * 255, uv_coord.x * 255., uv_coord.y * 255.);
}

void
main()
{
   ivec2 image_pixel_pos = ivec2(gl_GlobalInvocationID);
   float zoom            = 1.0;

   // Only use if we are within the pixel boundaries of the incoming image
   if (image_pixel_pos.x < 0 || image_pixel_pos.x >= int(push_constants.image_dimensions.x) ||
       image_pixel_pos.y < 0 || image_pixel_pos.y >= int(push_constants.image_dimensions.y))
   {
      return;
   }

   // Convert to -1 to +1
   vec2 uv_coord =
     vec2(((float(image_pixel_pos.x) / float(push_constants.image_dimensions.x)) * 2.0) - 1.0,
          ((float(image_pixel_pos.y) / float(push_constants.image_dimensions.y)) * 2.0) - 1.0);
   uint output_index = (image_pixel_pos.y * push_constants.image_dimensions.x) + image_pixel_pos.x;
   output_image.pixels[output_index] = uvec4(julia(uv_coord), 255);
}
