#version 430

// The workgroup size must match the program's size.
layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;

// Input image data
layout(binding = 0, std430) buffer input_buf
{
   uvec4 pixels[];
}
original_image;

layout(binding = 1, std430) buffer blur_buf
{
   uvec4 pixels[];
}
blurred_image;

// Output image data
layout(binding = 2, std430) buffer output_buf
{
   uvec4 pixels[];
}
output_image;

// Push constants block
layout(push_constant) uniform constants
{
   uvec4 image_props; // x=width, y=height, z=number_of_components, w=[unused]
}
pushConsts;

uvec4
diff(uvec4 a, uvec4 b)
{
   uvec4 result;
   if (a.x > b.x)
   {
      result.x = a.x - b.x;
   }
   else
   {
      result.x = b.x - a.x;
   }
   if (a.y > b.y)
   {
      result.y = a.y - b.y;
   }
   else
   {
      result.y = b.y - a.y;
   }
   if (a.z > b.z)
   {
      result.z = a.z - b.z;
   }
   else
   {
      result.z = b.z - a.z;
   }
   if (a.w > b.w)
   {
      result.w = a.w - b.w;
   }
   else
   {
      result.w = b.w - a.w;
   }
   return result * 3;
}

void
main()
{
   // Only use if we are within the pixel boundaries of the incoming image
   if (gl_GlobalInvocationID.x >= pushConsts.image_props.x ||
       gl_GlobalInvocationID.y >= pushConsts.image_props.y)
   {
      return;
   }

   uint pixel_index =
     (gl_GlobalInvocationID.y * pushConsts.image_props.x) + gl_GlobalInvocationID.x;
   output_image.pixels[pixel_index] =
     diff(original_image.pixels[pixel_index], blurred_image.pixels[pixel_index]);
}
