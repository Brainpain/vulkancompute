#version 430

// The workgroup size must match the program's size.
layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;

// Input image data
layout(binding = 0, std430) buffer in_image
{
   uvec4 pixels[];
}
input_image;

layout(binding = 1, std430) buffer in_buf
{
   float data[];
}
input_gaussian;

// Output image data
layout(binding = 2, std430) buffer out_buf
{
   uvec4 pixels[];
}
output_image;

// Push constants block
layout(push_constant) uniform constants
{
   uvec4 image_props; // x=width, y=height, z=number_of_components, w=[unused]
   int   kernel_width;
}
push_constants;

void
main()
{
   ivec2 image_dimensions = ivec2(push_constants.image_props);
   ivec2 image_pixel_pos  = ivec2(gl_GlobalInvocationID);
   int   kernel_width     = push_constants.kernel_width;
   int   half_value       = (kernel_width - 1) / 2;

   // Only use if we are within the pixel boundaries of the incoming image
   if (image_pixel_pos.x < 0 ||
       image_pixel_pos.x > (image_dimensions.x - 1) ||
       image_pixel_pos.y < 0 ||
       image_pixel_pos.y > (image_dimensions.y - 1))
   {
      return;
   }
   int output_index = (image_pixel_pos.y * image_dimensions.x) + image_pixel_pos.x;

   vec4 pixel_values = {0.0, 0.0, 0.0, 0.0};
   for (int x_mod = 0; x_mod < kernel_width; ++x_mod)
   {
      for (int y_mod = 0; y_mod < kernel_width; ++y_mod)
      {
         ivec2 input_image_pos    = ivec2(image_pixel_pos.x + (x_mod - half_value),
                                       image_pixel_pos.y + (y_mod - half_value));
         float cur_gaussian_value = input_gaussian.data[(y_mod * kernel_width) + x_mod];

         // If pixel falls outside edge of image, just duplicate the edge pixel
         if (input_image_pos.x < 0 ||
             input_image_pos.x > (image_dimensions.x - 1) ||
             input_image_pos.y < 0 ||
             input_image_pos.y > (image_dimensions.y - 1))
         {
            continue;
         }
         uint input_index = (input_image_pos.y * image_dimensions.x) + input_image_pos.x;
         pixel_values += input_image.pixels[input_index] * cur_gaussian_value;
      }
   }
   output_image.pixels[output_index] = uvec4(pixel_values);
}
