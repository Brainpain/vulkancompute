#version 430
layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

layout(binding = 0, std430) buffer input_buf
{
    int data[16384];
} inArray;

layout(binding = 1, std430) buffer output_buf
{
    int data[16384];
} outArray;

void main()
{
    outArray.data[gl_GlobalInvocationID.x] = inArray.data[gl_GlobalInvocationID.x];
}