//
// Copyright (c) Mark Young, 2021,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#pragma once

#include "vulkan_compute_buffer.hpp"
#include "vulkan_compute_device.hpp"
#include "vulkan_compute_log.hpp"
#include "vulkan_compute_shader.hpp"

class VulkanComputePipeline
{
 public:
   VulkanComputePipeline(const std::string         &name,
                         const VulkanComputeDevice *parent,
                         VkShaderModule             shader_module,
                         const char                *shader_main_name,
                         uint64_t                   push_constant_size);
   virtual ~VulkanComputePipeline();

   void BindBuffer(VulkanComputeBuffer *buffer, uint32_t access_flags, uint32_t index);
   void UnbindBuffer(const VulkanComputeBuffer *buffer);

   void PushConstantSize(uint64_t size);
   bool UploadToDevice();
   bool UnloadFromDevice();

   void Bind(VkCommandBuffer vk_command_buffer);

   VkShaderModule GetShaderModule()
   {
      return vulkan_shader_module_;
   }

   VkPipeline GetVkPipeline() const
   {
      return vulkan_pipeline_;
   }

   VkPipelineLayout GetVkPipelineLayout() const
   {
      return vulkan_pipeline_layout_;
   }

 private:
   struct PipelineBufferInfo
   {
      const VulkanComputeBuffer *buffer;
      bool                       bound;
      uint32_t                   binding_index;
      uint32_t                   access_flags;
   };

   const VulkanComputeDevice      *parent_device_ = nullptr;
   std::string                     name_;
   VkShaderModule                  vulkan_shader_module_;
   std::string                     shader_main_name_;
   uint64_t                        push_constant_size_ = 0ULL;
   std::vector<PipelineBufferInfo> active_buffer_infos_;
   VkDescriptorSetLayout           vulkan_desc_set_layout_ = VK_NULL_HANDLE;
   VkPipelineLayout                vulkan_pipeline_layout_ = VK_NULL_HANDLE;
   VkPipeline                      vulkan_pipeline_        = VK_NULL_HANDLE;
   VkDescriptorPool                vulkan_desc_pool_       = VK_NULL_HANDLE;
   VkDescriptorSet                 vulkan_descriptor_set_  = VK_NULL_HANDLE;
};
