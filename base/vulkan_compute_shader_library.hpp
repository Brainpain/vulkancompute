//
// Copyright (c) Mark Young, 2018-2020,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#pragma once

#include "vulkan_compute_shader.hpp"

#include <cstdint>
#include <string>
#include <unordered_map>

class VulkanComputeShaderLibrary : public VulkanComputeShader::Owner
{
 public:
   VulkanComputeShaderLibrary(const VulkanComputeDevice *compue_device);
   ~VulkanComputeShaderLibrary();

   VulkanComputeShader *LoadFromSpirvFile(const char *spirv_file_name);
   void                 FreeVulkanComputeShader(VulkanComputeShader *shader);

   // VulkanComputeShader owner interface
   const VulkanComputeDevice *GetComputeDevice(void) override
   {
      return compute_device_;
   }
   int64_t GenerateHashFromString(const std::string &s) const override
   {
      std::hash<std::string> string_hasher;
      return static_cast<int64_t>(string_hasher(s));
   }
   int64_t GenerateHashFromIntVector(const std::vector<uint32_t> &int_vec) const override
   {
      std::hash<uint32_t> uint_hasher;
      int64_t             result = 0;
      for (auto it : int_vec)
      {
         result = result * 31 + static_cast<int64_t>(uint_hasher(it));
      }
      return result;
   }
   VulkanComputeShader *FindShaderByHash(int64_t hash);

 private:
   const VulkanComputeDevice                         *compute_device_;
   std::unordered_map<int64_t, VulkanComputeShader *> shader_map_;
};