//
// Copyright (c) Mark Young, 2018-2020,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "vulkan_compute_shader_library.hpp"

#include "vulkan_compute_log.hpp"

VulkanComputeShaderLibrary::VulkanComputeShaderLibrary(const VulkanComputeDevice *compue_device)
  : compute_device_(compue_device)
{}

VulkanComputeShaderLibrary::~VulkanComputeShaderLibrary()
{
   for (auto it : shader_map_)
   {
      delete (it.second);
   }
}

VulkanComputeShader *
VulkanComputeShaderLibrary::LoadFromSpirvFile(const char *spirv_file_name)
{
   VulkanComputeShader *compute_shader = new VulkanComputeShader(this);
   if (nullptr == compute_shader)
   {
      std::string error = "VulkanComputeShaderLibrary::LoadFromSpirvFile failed "
                          "creating shader for ";
      error += spirv_file_name;
      LogFatalError(error);
      return nullptr;
   }
   if (!compute_shader->LoadFromSpirvFile(spirv_file_name))
   {
      std::string error = "VulkanComputeShaderLibrary::LoadFromSpirvFile failed loading ";
      error += spirv_file_name;
      LogFatalError(error);
      return nullptr;
   }
   shader_map_[compute_shader->Hash()] = compute_shader;
   return compute_shader;
}

void
VulkanComputeShaderLibrary::FreeVulkanComputeShader(VulkanComputeShader *shader)
{
   shader_map_.erase(shader->Hash());
}

VulkanComputeShader *
VulkanComputeShaderLibrary::FindShaderByHash(int64_t hash)
{
   auto iter = shader_map_.find(hash);
   if (iter == shader_map_.end())
   {
      return nullptr;
   }
   return iter->second;
}
