//
// Copyright (c) Mark Young, 2018-2020,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "vulkan_compute_shader.hpp"

#include "vulkan_compute_device.hpp"
#include "vulkan_compute_log.hpp"

#include <cassert>
#include <fstream>
#include <iterator>

VulkanComputeShader::VulkanComputeShader(Owner *owner)
  : _owner(owner)
{
   vulkan_shader_module_ = VK_NULL_HANDLE;
   shader_hash_          = -1;
}

VulkanComputeShader::~VulkanComputeShader()
{
   FreeVkShaderModule();
}

bool
VulkanComputeShader::LoadFromSpirvFile(const char *spirv_file_name)
{
   try
   {
      bool                  success = true;
      std::vector<uint32_t> shader_spv;
      std::streampos        fileSize;
      std::string           full_shader_name = "resources/shaders/";
      full_shader_name += spirv_file_name;

      std::ifstream if_spirv_file(full_shader_name, std::ios::binary);
      if (if_spirv_file.fail())
      {
         std::string error = "VulkanComputeShader::LoadFromSpirvFile failed loading ";
         error += full_shader_name;
         LogFatalError(error);
         throw 0;
      }
      if_spirv_file.unsetf(std::ios::skipws);
      if_spirv_file.seekg(0, std::ios::end);
      fileSize = if_spirv_file.tellg();
      if_spirv_file.seekg(0, std::ios::beg);

      uint32_t cur_value = 0;
      while (if_spirv_file.read((char *)&cur_value, sizeof(uint32_t)))
      {
         shader_spv.push_back(cur_value);
      }
      assert(shader_spv.size() > 0);
      shader_hash_ = _owner->GenerateHashFromIntVector(shader_spv);

      VkShaderModuleCreateInfo moduleCreateInfo = {};
      moduleCreateInfo.sType                    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
      moduleCreateInfo.pNext                    = nullptr;
      moduleCreateInfo.flags                    = 0;
      moduleCreateInfo.codeSize                 = shader_spv.size() * sizeof(uint32_t);
      moduleCreateInfo.pCode                    = shader_spv.data();
      VkResult create_result = vkCreateShaderModule(_owner->GetComputeDevice()->GetVkDevice(),
                                                    &moduleCreateInfo,
                                                    nullptr,
                                                    &vulkan_shader_module_);
      if (VK_SUCCESS != create_result)
      {
         LogFatalError("VulkanComputeShader::LoadFromSpirvFile failed creating "
                       "VkShaderModule");
         success = false;
      }

      return success;
   }
   catch (...)
   {
      LogFatalError("VulkanComputeShader::LoadFromSpirvFile unhandled exception");
      return false;
   }
}

VkPipelineShaderStageCreateInfo
VulkanComputeShader::GetPipelineShaderStageInfo()
{
   VkPipelineShaderStageCreateInfo pipleline_shader_info = {};
   pipleline_shader_info.sType               = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
   pipleline_shader_info.pNext               = nullptr;
   pipleline_shader_info.pSpecializationInfo = nullptr;
   pipleline_shader_info.flags               = 0;
   pipleline_shader_info.stage               = VK_SHADER_STAGE_COMPUTE_BIT;
   pipleline_shader_info.pName               = "main";
   return pipleline_shader_info;
}

void
VulkanComputeShader::FreeVkShaderModule()
{
   if (VK_NULL_HANDLE != vulkan_shader_module_)
   {
      vkDestroyShaderModule(
        _owner->GetComputeDevice()->GetVkDevice(), vulkan_shader_module_, nullptr);
      vulkan_shader_module_ = VK_NULL_HANDLE;
      shader_hash_          = -1;
   }
}
