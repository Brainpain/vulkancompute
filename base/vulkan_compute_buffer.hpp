//
// Copyright (c) Mark Young, 2018-2020,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#pragma once

#include <string>
#include <vulkan/vulkan_core.h>

class VulkanComputeDevice;

class VulkanComputeBuffer
{
 public:
   // Flags used to identify expected buffer behavior
   enum VulkanComputeBufferAccessFlags
   {
      VULKAN_COMPUTE_BUFFER_ACCESS_FLAG_INVALID = 0,
      VULKAN_COMPUTE_BUFFER_ACCESS_FLAG_INPUT   = 0x1,
      VULKAN_COMPUTE_BUFFER_ACCESS_FLAG_OUTPUT  = 0x2,
   };

   VulkanComputeBuffer(const std::string &name, VulkanComputeDevice *compute_device, size_t size);
   ~VulkanComputeBuffer();

   const std::string Name() const
   {
      return name_;
   }

   // Use this for simple writes.  This maps the buffer, copies the data, unmaps,
   // and flushes.
   bool WriteData(size_t size, size_t offset, const uint8_t *data);

   // Use this when you want to manage your own writes and flusing behavior.
   uint8_t *MapMemoryPointer(size_t size, size_t offset);
   void     UnmapMemoryPointer();
   void     FlushMemory(size_t size, size_t offset);

   inline void SetCurrentDescriptorIndex(uint32_t descriptor_index)
   {
      vulkan_descriptor_index_ = descriptor_index;
   }
   VkDescriptorSetLayoutBinding GetVkDescriptorSetLayout() const;
   VkDescriptorBufferInfo       GetVkDescriptorBufferInfo() const;
   VkWriteDescriptorSet         GetVkWriteDescriptorSet(VkDescriptorSet         vulkan_desc_set,
                                                        VkDescriptorBufferInfo *vulkan_desc_buf_info) const;

   void TransitionFromOutputToInput(VkCommandBuffer command_buffer);

 private:
   // Selects the appropriate memory type for this buffer
   bool SelectMemoryType(VkPhysicalDeviceMemoryProperties &vulkan_memory_props,
                         size_t                            size,
                         uint32_t                          required_flags,
                         uint32_t                          desired_flags,
                         uint32_t                         &memory_type_index);

   std::string                name_;
   const VulkanComputeDevice *parent_device_;
   VkDevice                   vulkan_device_;
   VkBuffer                   vulkan_buffer_;
   VkMemoryRequirements       vulkan_buffer_memory_requirements_;
   VkDeviceMemory             vulkan_device_memory_;
   bool                       is_coherent_;
   bool                       is_mapped_;
   uint32_t                   vulkan_memory_type_index_;
   uint32_t                   vulkan_memory_properties_;
   size_t                     memory_size_;
   uint32_t                   memory_alignment_;
   uint32_t                   vulkan_descriptor_index_;
};