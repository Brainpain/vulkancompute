//
// Copyright (c) Mark Young, 2021,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "vulkan_compute_pipeline.hpp"

#include <sstream>

VulkanComputePipeline::VulkanComputePipeline(const std::string         &name,
                                             const VulkanComputeDevice *parent,
                                             VkShaderModule             shader_module,
                                             const char                *shader_main_name,
                                             uint64_t                   push_constant_size)
  : parent_device_(parent)
  , name_(name)
  , vulkan_shader_module_(shader_module)
  , shader_main_name_(shader_main_name)
  , push_constant_size_(push_constant_size)
{}

VulkanComputePipeline::~VulkanComputePipeline()
{
   active_buffer_infos_.clear();
}

void
VulkanComputePipeline::BindBuffer(VulkanComputeBuffer *buffer,
                                  uint32_t             access_flags,
                                  uint32_t             index)
{
   if (nullptr != buffer)
   {
      for (uint32_t buf = 0; buf < static_cast<uint32_t>(active_buffer_infos_.size()); ++buf)
      {
         if (active_buffer_infos_[buf].bound && active_buffer_infos_[buf].binding_index == index)
         {
            std::string warning_message = "VulkanComputePipeline(";
            warning_message += name_;
            warning_message += ")::BindBuffer - another buffer ";
            warning_message += active_buffer_infos_[buf].buffer->Name();
            warning_message += " (";
            std::stringstream str_stream;
            str_stream << active_buffer_infos_[buf].buffer;
            warning_message += str_stream.str();
            warning_message += ") is already bound to index ";
            warning_message += std::to_string(index);
            warning_message += " being requested for buffer ";
            warning_message += buffer->Name();
            warning_message += " (";
            std::stringstream str_stream2;
            str_stream2 << buffer;
            warning_message += str_stream2.str();
            warning_message += ").  Perhaps unbind the previous one first.";
            LogWarning(warning_message);
         }
      }

      PipelineBufferInfo compute_buffer_usage;
      compute_buffer_usage.buffer        = buffer;
      compute_buffer_usage.access_flags  = access_flags;
      compute_buffer_usage.bound         = true;
      compute_buffer_usage.binding_index = index;
      active_buffer_infos_.push_back(compute_buffer_usage);
      buffer->SetCurrentDescriptorIndex(index);
   }
}

void
VulkanComputePipeline::UnbindBuffer(const VulkanComputeBuffer *buffer)
{
   if (nullptr != buffer)
   {
      for (uint32_t buf = 0; buf < static_cast<uint32_t>(active_buffer_infos_.size()); ++buf)
      {
         if (buffer == active_buffer_infos_[buf].buffer)
         {
            if (!active_buffer_infos_[buf].bound)
            {
               std::string warning_message = "VulkanComputePipeline(";
               warning_message += name_;
               warning_message += ")::UnbindBuffer - called on buffer ";
               warning_message += active_buffer_infos_[buf].buffer->Name();
               warning_message += " (";
               std::stringstream str_stream;
               str_stream << active_buffer_infos_[buf].buffer;
               warning_message += str_stream.str();
               warning_message += ") that is not bound.  Ignoring.";
               LogWarning(warning_message);
            }
            else
            {
               active_buffer_infos_[buf].bound         = false;
               active_buffer_infos_[buf].binding_index = 0;
               active_buffer_infos_[buf].access_flags  = 0;
            }
            break;
         }
      }
   }
}

bool
VulkanComputePipeline::UploadToDevice()
{
   // Setup the descriptor set layout info
   std::vector<VkDescriptorSetLayoutBinding> desc_set_layout_bindings;
   uint32_t                                  max_binding = 0;
   VkPushConstantRange                       push_constant_range;
   for (uint buf = 0; buf < active_buffer_infos_.size(); ++buf)
   {
      if (active_buffer_infos_[buf].bound && max_binding < active_buffer_infos_[buf].binding_index)
      {
         max_binding = active_buffer_infos_[buf].binding_index;
      }
   }
   desc_set_layout_bindings.resize(max_binding + 1);
   for (uint buf = 0; buf < active_buffer_infos_.size(); ++buf)
   {
      if (active_buffer_infos_[buf].bound)
      {
         desc_set_layout_bindings[active_buffer_infos_[buf].binding_index] =
           active_buffer_infos_[buf].buffer->GetVkDescriptorSetLayout();
      }
   }

   // Create the descriptor set layout
   VkDescriptorSetLayoutCreateInfo desc_set_layout_create_info = {};
   desc_set_layout_create_info.sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
   desc_set_layout_create_info.flags        = 0;
   desc_set_layout_create_info.bindingCount = desc_set_layout_bindings.size();
   desc_set_layout_create_info.pBindings    = desc_set_layout_bindings.data();
   if (VK_SUCCESS !=
       vkCreateDescriptorSetLayout(
         parent_device_->GetVkDevice(), &desc_set_layout_create_info, 0, &vulkan_desc_set_layout_))
   {
      std::string error_message = "VulkanComputePipeline(";
      error_message += name_;
      error_message += ")::UploadToDevice - Failed to create descriptor set layout";
      LogError(error_message);
      return false;
   }

   // Create the pipeline layout now.
   VkPipelineLayoutCreateInfo pipeline_layout_create_info = {};
   pipeline_layout_create_info.sType          = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
   pipeline_layout_create_info.flags          = 0;
   pipeline_layout_create_info.setLayoutCount = 1;
   pipeline_layout_create_info.pSetLayouts    = &vulkan_desc_set_layout_;
   if (push_constant_size_ > 0)
   {
      push_constant_range.offset                         = 0;
      push_constant_range.size                           = push_constant_size_;
      push_constant_range.stageFlags                     = VK_SHADER_STAGE_COMPUTE_BIT;
      pipeline_layout_create_info.pushConstantRangeCount = 1;
      pipeline_layout_create_info.pPushConstantRanges    = &push_constant_range;
   }
   if (VK_SUCCESS !=
       vkCreatePipelineLayout(
         parent_device_->GetVkDevice(), &pipeline_layout_create_info, 0, &vulkan_pipeline_layout_))
   {
      std::string error_message = "VulkanComputePipeline(";
      error_message += name_;
      error_message += ")::UploadToDevice - Failed to create Vulkan pipeline layout";
      LogError(error_message);
      return false;
   }

   VkComputePipelineCreateInfo compute_pipeline_create_info = {};
   compute_pipeline_create_info.sType        = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
   compute_pipeline_create_info.flags        = 0;
   compute_pipeline_create_info.layout       = vulkan_pipeline_layout_;
   compute_pipeline_create_info.stage        = {};
   compute_pipeline_create_info.stage.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
   compute_pipeline_create_info.stage.stage  = VK_SHADER_STAGE_COMPUTE_BIT;
   compute_pipeline_create_info.stage.module = vulkan_shader_module_;
   compute_pipeline_create_info.stage.pName  = shader_main_name_.c_str();

   if (VK_SUCCESS != vkCreateComputePipelines(parent_device_->GetVkDevice(),
                                              VK_NULL_HANDLE,
                                              1,
                                              &compute_pipeline_create_info,
                                              nullptr,
                                              &vulkan_pipeline_))
   {
      std::string error_message = "VulkanComputePipeline(";
      error_message += name_;
      error_message += ")::UploadToDevice - Failed to create Vulkan pipeline";
      LogError(error_message);
      return false;
   }

   parent_device_->SetObjectName(
     VK_OBJECT_TYPE_PIPELINE, reinterpret_cast<uint64_t>(vulkan_pipeline_), name_.c_str());

   VkDescriptorPoolSize desc_pool_size = {};
   desc_pool_size.type                 = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
   desc_pool_size.descriptorCount      = desc_set_layout_bindings.size();

   VkDescriptorPoolCreateInfo desc_pool_create_info = {};
   desc_pool_create_info.sType                      = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
   desc_pool_create_info.maxSets                    = 1;
   desc_pool_create_info.poolSizeCount              = 1;
   desc_pool_create_info.pPoolSizes                 = &desc_pool_size;
   desc_pool_create_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;

   if (VK_SUCCESS !=
       vkCreateDescriptorPool(
         parent_device_->GetVkDevice(), &desc_pool_create_info, 0, &vulkan_desc_pool_))
   {
      std::string error_message = "VulkanComputePipeline(";
      error_message += name_;
      error_message += ")::UploadToDevice - Failed to create Vulkan descriptor pool";
      LogError(error_message);
      return false;
   }

   VkDescriptorSetAllocateInfo desc_set_alloc_info = {};
   desc_set_alloc_info.sType                       = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
   desc_set_alloc_info.descriptorPool              = vulkan_desc_pool_;
   desc_set_alloc_info.descriptorSetCount          = 1;
   desc_set_alloc_info.pSetLayouts                 = &vulkan_desc_set_layout_;
   if (VK_SUCCESS != vkAllocateDescriptorSets(parent_device_->GetVkDevice(),
                                              &desc_set_alloc_info,
                                              &vulkan_descriptor_set_))
   {
      std::string error_message = "VulkanComputePipeline(";
      error_message += name_;
      error_message += ")::UploadToDevice - Failed to allocate Vulkan descriptor sets";
      LogError(error_message);
      return false;
   }

   std::vector<VkDescriptorBufferInfo> desc_buffer_info;
   std::vector<VkWriteDescriptorSet>   write_desc_set;
   desc_buffer_info.resize(max_binding + 1);
   write_desc_set.resize(max_binding + 1);
   for (uint buf = 0; buf < active_buffer_infos_.size(); ++buf)
   {
      if (active_buffer_infos_[buf].bound)
      {
         uint32_t                   index       = active_buffer_infos_[buf].binding_index;
         const VulkanComputeBuffer *comp_buffer = active_buffer_infos_[buf].buffer;
         VkDescriptorBufferInfo    *buf_ptr     = &(*(desc_buffer_info.begin() + index));
         desc_buffer_info[index]                = comp_buffer->GetVkDescriptorBufferInfo();
         write_desc_set[index] =
           comp_buffer->GetVkWriteDescriptorSet(vulkan_descriptor_set_, buf_ptr);
      }
   }
   vkUpdateDescriptorSets(
     parent_device_->GetVkDevice(), write_desc_set.size(), write_desc_set.data(), 0, nullptr);
   return true;
}

bool
VulkanComputePipeline::UnloadFromDevice()
{
   bool success = true;
   if (VK_NULL_HANDLE != vulkan_descriptor_set_)
   {
      if (VK_SUCCESS !=
          vkFreeDescriptorSets(
            parent_device_->GetVkDevice(), vulkan_desc_pool_, 1, &vulkan_descriptor_set_))
      {
         std::string error_message = "VulkanComputePipeline(";
         error_message += name_;
         error_message += ")::UnloadFromDevice - Failed to free Vulkan descriptor sets";
         LogError(error_message);
         success = false;
      }
   }

   if (VK_NULL_HANDLE != vulkan_desc_pool_)
   {
      vkDestroyDescriptorPool(parent_device_->GetVkDevice(), vulkan_desc_pool_, nullptr);
   }

   if (VK_NULL_HANDLE != vulkan_pipeline_)
   {
      vkDestroyPipeline(parent_device_->GetVkDevice(), vulkan_pipeline_, nullptr);
   }

   if (VK_NULL_HANDLE != vulkan_pipeline_layout_)
   {
      vkDestroyPipelineLayout(parent_device_->GetVkDevice(), vulkan_pipeline_layout_, nullptr);
   }

   if (VK_NULL_HANDLE != vulkan_desc_set_layout_)
   {
      vkDestroyDescriptorSetLayout(parent_device_->GetVkDevice(), vulkan_desc_set_layout_, nullptr);
   }
   return success;
}

void
VulkanComputePipeline::Bind(VkCommandBuffer command_buffer)
{
   vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_COMPUTE, vulkan_pipeline_);

   vkCmdBindDescriptorSets(command_buffer,
                           VK_PIPELINE_BIND_POINT_COMPUTE,
                           vulkan_pipeline_layout_,
                           0,
                           1,
                           &vulkan_descriptor_set_,
                           0,
                           0);
}
