//
// Copyright (c) Mark Young, 2018-2020,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "vulkan_compute_device.hpp"

#include "vulkan_compute_log.hpp"
#include "vulkan_compute_shader_library.hpp"

#include <algorithm>
#include <cstring>
#include <limits>
#include <sstream>
#include <vector>

VKAPI_ATTR VkBool32 VKAPI_CALL
debug_messenger_callback(VkDebugUtilsMessageSeverityFlagBitsEXT      messageSeverity,
                         VkDebugUtilsMessageTypeFlagsEXT             messageType,
                         const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
                         void                                       *pUserData)
{
   std::string message;
   if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT)
   {
      message += "GENERAL";
   }
   else
   {
      if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)
      {
         message += "VALIDATION";
      }
      if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT)
      {
         if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)
         {
            message += "|";
         }
         message += "PERFORMANCE";
      }
   }

   message += " - Message Id Number: ";
   message += std::to_string(pCallbackData->messageIdNumber);
   if (nullptr != pCallbackData->pMessageIdName)
   {
      message += " | Message Id Name: ";
      message += pCallbackData->pMessageIdName;
   }
   message += "\n\t";
   message += pCallbackData->pMessage;
   message += "\n";
   if (pCallbackData->objectCount > 0)
   {
      message += "\t\tObjects - ";
      message += std::to_string(pCallbackData->objectCount);
      message += "\n";
      for (uint32_t object = 0; object < pCallbackData->objectCount; ++object)
      {
         message += "\t\t\tObject[";
         message += std::to_string(object);
         message += "] - ";
         message +=
           std::to_string(static_cast<uint64_t>(pCallbackData->pObjects[object].objectType));
         message += ", Handle ";
         std::ostringstream oss_handle;
         oss_handle << std::hex
                    << reinterpret_cast<const void *>(pCallbackData->pObjects[object].objectHandle);
         message += oss_handle.str();
         if (NULL != pCallbackData->pObjects[object].pObjectName &&
             strlen(pCallbackData->pObjects[object].pObjectName) > 0)
         {
            message += ", Name \"";
            message += pCallbackData->pObjects[object].pObjectName;
            message += "\"\n";
         }
         else
         {
            message += "\n";
         }
      }
   }
   if (pCallbackData->cmdBufLabelCount > 0)
   {
      message += "\t\tCommand Buffer Labels - ";
      message += std::to_string(pCallbackData->cmdBufLabelCount);
      for (uint32_t cmd_buf_label = 0; cmd_buf_label < pCallbackData->cmdBufLabelCount;
           ++cmd_buf_label)
      {
         message += "\t\t\tLabel[";
         message += std::to_string(cmd_buf_label);
         message += "] - \"";
         message += pCallbackData->pCmdBufLabels[cmd_buf_label].pLabelName;
         message += "\" { ";
         message += std::to_string(pCallbackData->pCmdBufLabels[cmd_buf_label].color[0]);
         message += ", ";
         message += std::to_string(pCallbackData->pCmdBufLabels[cmd_buf_label].color[1]);
         message += ", ";
         message += std::to_string(pCallbackData->pCmdBufLabels[cmd_buf_label].color[2]);
         message += ", ";
         message += std::to_string(pCallbackData->pCmdBufLabels[cmd_buf_label].color[3]);
      }
   }

   if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
   {
      std::cout << message << std::endl;
      std::cout.flush();
   }
   else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
   {
      LogInfo(message);
   }
   else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
   {
      LogWarning(message);
   }
   else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
   {
      LogError(message);
   }

   // Don't bail out, but keep going.
   return false;
}

VulkanComputeDevice::VulkanComputeDevice(const std::string &application_name)
{
   vulkan_instance_              = VK_NULL_HANDLE;
   vulkan_debug_utils_messenger_ = VK_NULL_HANDLE;
   vulkan_physical_device_       = VK_NULL_HANDLE;
   vulkan_device_                = VK_NULL_HANDLE;
   vulkan_queue_                 = VK_NULL_HANDLE;
   vulkan_command_pool_          = VK_NULL_HANDLE;

   pfnVkDestroyDebugUtilsMessengerEXT_ = nullptr;
   pfnVkCmdPipelineBarrier2_           = nullptr;
   pfnVkSetDebugUtilsObjectNameEXT_    = nullptr;

   InitAndSelectDevice(application_name);
   shader_library_ = new VulkanComputeShaderLibrary(this);
}

VulkanComputeDevice::~VulkanComputeDevice()
{
   if (VK_NULL_HANDLE != vulkan_command_pool_)
   {
      vkDestroyCommandPool(vulkan_device_, vulkan_command_pool_, nullptr);
      vulkan_command_pool_ = VK_NULL_HANDLE;
   }

   while (active_compute_buffer_info_.size() > 0)
   {
      DestroyBuffer(active_compute_buffer_info_[0].buffer);
   }
   vulkan_queue_ = VK_NULL_HANDLE;
   if (nullptr != shader_library_)
   {
      delete shader_library_;
      shader_library_ = nullptr;
   }
   if (VK_NULL_HANDLE != vulkan_device_)
   {
      vkDestroyDevice(vulkan_device_, nullptr);
      vulkan_device_ = VK_NULL_HANDLE;
   }
   vulkan_physical_device_ = VK_NULL_HANDLE;
   if (VK_NULL_HANDLE != vulkan_instance_)
   {
      if (VK_NULL_HANDLE != vulkan_debug_utils_messenger_ &&
          nullptr != pfnVkDestroyDebugUtilsMessengerEXT_)
      {
         pfnVkDestroyDebugUtilsMessengerEXT_(
           vulkan_instance_, vulkan_debug_utils_messenger_, nullptr);
         vulkan_debug_utils_messenger_    = VK_NULL_HANDLE;
         pfnVkSetDebugUtilsObjectNameEXT_ = nullptr;
      }
      vkDestroyInstance(vulkan_instance_, nullptr);
      vulkan_instance_ = VK_NULL_HANDLE;
   }
}

bool
VulkanComputeDevice::DebugUtilsPresent()
{
   uint32_t ext_count = 0;
   vkEnumerateInstanceExtensionProperties(nullptr, &ext_count, nullptr);
   if (ext_count > 0)
   {
      std::vector<VkExtensionProperties> ext_props;
      ext_props.resize(ext_count);
      vkEnumerateInstanceExtensionProperties(nullptr, &ext_count, ext_props.data());
      for (uint32_t iii = 0; iii < ext_count; ++iii)
      {
         if (!strcmp(ext_props[iii].extensionName, VK_EXT_DEBUG_UTILS_EXTENSION_NAME))
         {
            return true;
         }
      }
   }
   return false;
}

const char g_validation_layer_name[VK_MAX_EXTENSION_NAME_SIZE] =
  "VK_LAYER_LUNARG_standard_validation";

bool
VulkanComputeDevice::ValidationLayerPresent()
{
   uint32_t layer_count = 0;
   vkEnumerateInstanceLayerProperties(&layer_count, nullptr);
   if (layer_count > 0)
   {
      std::vector<VkLayerProperties> layer_props;
      layer_props.resize(layer_count);
      vkEnumerateInstanceLayerProperties(&layer_count, layer_props.data());
      for (uint32_t iii = 0; iii < layer_count; ++iii)
      {
         if (!strcmp(layer_props[iii].layerName, g_validation_layer_name))
         {
            return true;
         }
      }
   }
   return false;
}

void
VulkanComputeDevice::InitAndSelectDevice(const std::string &application_name)
{
   vulkan_instance_ = VK_NULL_HANDLE;

   const char  debug_utils_extension_name[] = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
   const char *extension_list[] = {reinterpret_cast<const char *>(&debug_utils_extension_name[0])};
   uint32_t    extension_count  = 1;
   const char *layer_list[]     = {reinterpret_cast<const char *>(&g_validation_layer_name[0])};
   uint32_t    layer_count      = 1;

   uint32_t                       instance_version = VK_MAKE_VERSION(1, 0, 0);
   PFN_vkEnumerateInstanceVersion enumerate_instance_version =
     (PFN_vkEnumerateInstanceVersion)vkGetInstanceProcAddr(nullptr, "vkEnumerateInstanceVersion");
   if (enumerate_instance_version != nullptr &&
       VK_SUCCESS != enumerate_instance_version(&instance_version))
   {
      LogInfo(
        "InitAndSelectDevice: Failed attempting to query newer API version.  Using Vulkan 1.0");
      major_version_ = 1;
      minor_version_ = 0;
   }
   else
   {
      major_version_ = VK_VERSION_MAJOR(instance_version);
      minor_version_ = VK_VERSION_MINOR(instance_version);
   }

   std::string       engine_name         = "Vulkan Compute Samples";
   VkApplicationInfo app_info            = {};
   app_info.sType                        = VK_STRUCTURE_TYPE_APPLICATION_INFO;
   app_info.apiVersion                   = VK_MAKE_VERSION(major_version_, minor_version_, 0);
   app_info.pApplicationName             = application_name.c_str();
   app_info.pEngineName                  = engine_name.c_str();
   VkInstanceCreateInfo inst_create_info = {};
   inst_create_info.sType                = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
   inst_create_info.pApplicationInfo     = &app_info;
   if (DebugUtilsPresent())
   {
      LogInfo("InitAndSelectDevice: DebugUtilsPresent");
      inst_create_info.enabledExtensionCount   = extension_count;
      inst_create_info.ppEnabledExtensionNames = extension_list;
   }
   else
   {
      inst_create_info.enabledExtensionCount   = 0;
      inst_create_info.ppEnabledExtensionNames = nullptr;
   }
   if (ValidationLayerPresent())
   {
      LogInfo("InitAndSelectDevice: ValidationLayerPresent");
      inst_create_info.enabledLayerCount   = layer_count;
      inst_create_info.ppEnabledLayerNames = layer_list;
   }
   else
   {
      inst_create_info.enabledLayerCount   = 0;
      inst_create_info.ppEnabledLayerNames = nullptr;
   }

   VkResult result = vkCreateInstance(&inst_create_info, nullptr, &vulkan_instance_);
   if (VK_SUCCESS != result)
   {
      std::string error_message = "Failed to create Vulkan instance - ";
      error_message += std::to_string(result);
      LogFatalError(error_message);
   }

   if (DebugUtilsPresent())
   {
      // Setup VK_EXT_debug_utils function pointers always (we use them for
      // debug labels and names).
      PFN_vkCreateDebugUtilsMessengerEXT pfn_vkCreateDebugUtilsMessengerEXT =
        (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(vulkan_instance_,
                                                                  "vkCreateDebugUtilsMessengerEXT");
      pfnVkDestroyDebugUtilsMessengerEXT_ =
        (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
          vulkan_instance_, "vkDestroyDebugUtilsMessengerEXT");
      pfnVkSetDebugUtilsObjectNameEXT_ = (PFN_vkSetDebugUtilsObjectNameEXT)vkGetInstanceProcAddr(
        vulkan_instance_, "vkSetDebugUtilsObjectNameEXT");
      if (nullptr == pfn_vkCreateDebugUtilsMessengerEXT ||
          nullptr == pfnVkDestroyDebugUtilsMessengerEXT_ ||
          nullptr == pfnVkSetDebugUtilsObjectNameEXT_)
      {
         LogFatalError("GetProcAddr: Failed to init VK_EXT_debug_utils\n");
      }

      // Create a debug messenger
      VkDebugUtilsMessengerCreateInfoEXT dbg_messenger_create_info = {};
      dbg_messenger_create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
      dbg_messenger_create_info.pNext = nullptr;
      dbg_messenger_create_info.flags = 0;
      dbg_messenger_create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                                  VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
      dbg_messenger_create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                                              VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                                              VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
      dbg_messenger_create_info.pfnUserCallback = debug_messenger_callback;
      dbg_messenger_create_info.pUserData       = this;

      if (VK_SUCCESS !=
          pfn_vkCreateDebugUtilsMessengerEXT(
            vulkan_instance_, &dbg_messenger_create_info, nullptr, &vulkan_debug_utils_messenger_))
      {
         LogFatalError("vkCreateDebugUtilsMessengerEXT: Failed to create messenger\n");
      }
   }

   // Get info on all physical devices
   uint32_t                      phys_device_count = 0;
   std::vector<VkPhysicalDevice> physical_devices;
   if (VK_SUCCESS != vkEnumeratePhysicalDevices(vulkan_instance_, &phys_device_count, nullptr) ||
       0 == phys_device_count)
   {
      LogFatalError("Failed to make call to determine if there are any Vulkan "
                    "Physical devices");
   }
   physical_devices.resize(phys_device_count);
   if (VK_SUCCESS != vkEnumeratePhysicalDevices(
                       vulkan_instance_, &phys_device_count, physical_devices.data()) ||
       0 == phys_device_count)
   {
      LogFatalError("Failed to find any Vulkan Physical devices");
   }

   uint32_t    largest_work_count = 0;
   std::string device_name;
   for (uint32_t dev = 0; dev < phys_device_count; ++dev)
   {
      uint32_t                             phys_dev_num_queue_family_props = 0;
      std::vector<VkQueueFamilyProperties> queue_family_properties;
      vkGetPhysicalDeviceQueueFamilyProperties(
        physical_devices[dev], &phys_dev_num_queue_family_props, nullptr);
      if (phys_dev_num_queue_family_props > 0)
      {
         queue_family_properties.resize(phys_dev_num_queue_family_props);
         vkGetPhysicalDeviceQueueFamilyProperties(
           physical_devices[dev], &phys_dev_num_queue_family_props, queue_family_properties.data());

         bool     supports_compute = false;
         uint32_t supporting_queue = 0;
         for (uint32_t queue = 0; queue < phys_dev_num_queue_family_props; ++queue)
         {
            if (queue_family_properties[queue].queueFlags | VK_QUEUE_COMPUTE_BIT)
            {
               supports_compute = true;
               supporting_queue = queue;
               break;
            }
         }
         if (!supports_compute)
         {
            continue;
         }

         // Check to see if this physical device supports more work than any
         // previous device.
         VkPhysicalDeviceProperties phys_dev_properties = {};
         vkGetPhysicalDeviceProperties(physical_devices[dev], &phys_dev_properties);
         if (phys_dev_properties.limits.maxComputeWorkGroupCount[0] > largest_work_count)
         {
            vulkan_physical_device_    = physical_devices[dev];
            vulkan_queue_family_index_ = supporting_queue;
            largest_work_count         = phys_dev_properties.limits.maxComputeWorkGroupCount[0];
            memcpy(
              &vulkan_device_properties_, &phys_dev_properties, sizeof(VkPhysicalDeviceProperties));

            // Save the max group counts for later
            max_work_group_counts_[0] = phys_dev_properties.limits.maxComputeWorkGroupCount[0];
            max_work_group_counts_[1] = phys_dev_properties.limits.maxComputeWorkGroupCount[1];
            max_work_group_counts_[2] = phys_dev_properties.limits.maxComputeWorkGroupCount[2];
            device_name               = phys_dev_properties.deviceName;

            uint8_t device_major = VK_VERSION_MAJOR(phys_dev_properties.apiVersion);
            uint8_t device_minor = VK_VERSION_MINOR(phys_dev_properties.apiVersion);
            if (major_version_ > device_major)
            {
               major_version_ = device_major;
            }
            if (minor_version_ > device_minor)
            {
               minor_version_ = device_minor;
            }
         }
      }
   }
   if (VK_NULL_HANDLE == vulkan_physical_device_)
   {
      LogFatalError("Failed to find any Compute-capable Vulkan Physical devices");
   }
   LogInfo(device_name.c_str());

   vkGetPhysicalDeviceMemoryProperties(vulkan_physical_device_, &vulkan_memory_properties_);

   if (minor_version_ >= 3)
   {
      VkPhysicalDeviceVulkan13Features phys_dev_1_3_features {
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES, nullptr};
      VkPhysicalDeviceFeatures2 phys_dev_features2 {VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
                                                    &phys_dev_1_3_features};
      vkGetPhysicalDeviceFeatures2(vulkan_physical_device_, &phys_dev_features2);
      if (phys_dev_1_3_features.synchronization2)
      {
         supports_sync2_ = true;
      }
      if (phys_dev_features2.features.shaderFloat64)
      {
         supports_shader_float64_ = true;
      }
   }

   VkDeviceQueueCreateInfo device_queue_create_info = {};
   float                   queue_priority           = 0.f;
   device_queue_create_info.sType                   = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
   device_queue_create_info.queueCount              = 1;
   device_queue_create_info.queueFamilyIndex        = vulkan_queue_family_index_;
   device_queue_create_info.pQueuePriorities        = &queue_priority;
   VkDeviceCreateInfo device_create_info            = {};
   device_create_info.sType                         = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
   device_create_info.queueCreateInfoCount          = 1;
   device_create_info.pQueueCreateInfos             = &device_queue_create_info;

   VkPhysicalDeviceVulkan13Features phys_dev_1_3_features {
     VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES, nullptr};
   VkPhysicalDeviceFeatures2 phys_dev_features2 {VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
                                                 &phys_dev_1_3_features};
   device_create_info.pNext = &phys_dev_features2;
   if (supports_sync2_)
   {
      phys_dev_1_3_features.synchronization2 = true;
   }
   if (supports_shader_float64_)
   {
      phys_dev_features2.features.shaderFloat64 = true;
   }

   if (VK_SUCCESS !=
       vkCreateDevice(vulkan_physical_device_, &device_create_info, nullptr, &vulkan_device_))
   {
      LogFatalError("Failed to create Compute-capable Vulkan Logical device");
   }

   std::string log_version = "InitAndSelectDevice: Created device with Vulkan API version ";
   log_version += std::to_string(static_cast<unsigned int>(major_version_));
   log_version += ".";
   log_version += std::to_string(static_cast<unsigned int>(minor_version_));
   LogInfo(log_version);

   vkGetDeviceQueue(vulkan_device_, vulkan_queue_family_index_, 0, &vulkan_queue_);

   VkCommandPoolCreateInfo cmd_pool_create_info = {};
   cmd_pool_create_info.sType                   = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
   cmd_pool_create_info.queueFamilyIndex        = vulkan_queue_family_index_;
   if (VK_SUCCESS !=
       vkCreateCommandPool(vulkan_device_, &cmd_pool_create_info, 0, &vulkan_command_pool_))
   {
      LogFatalError("InitAndSelectDevice - Failed to create command pool");
   }

   if (supports_sync2_)
   {
      pfnVkCmdPipelineBarrier2_ =
        (PFN_vkCmdPipelineBarrier2)vkGetDeviceProcAddr(vulkan_device_, "vkCmdPipelineBarrier2");
      if (pfnVkCmdPipelineBarrier2_ == nullptr)
      {
         LogError("Failed to get vkCmdPipelineBarrier2");
      }
   }
}

VulkanComputeBuffer *
VulkanComputeDevice::CreateBuffer(const std::string &name, size_t size)
{
   if (size == 0)
   {
      LogError("Creating buffer with 0");
      return nullptr;
   }

   VulkanComputeBuffer *new_buffer = new VulkanComputeBuffer(name, this, size);
   if (nullptr != new_buffer)
   {
      ComputeBufferInfo compute_buffer_usage;
      compute_buffer_usage.buffer        = new_buffer;
      compute_buffer_usage.access_flags  = 0;
      compute_buffer_usage.bound         = false;
      compute_buffer_usage.binding_index = 0;
      active_compute_buffer_info_.push_back(compute_buffer_usage);
   }
   return new_buffer;
}

void
VulkanComputeDevice::DestroyBuffer(VulkanComputeBuffer *buffer)
{
   if (nullptr != buffer)
   {
      for (uint buf = 0; buf < active_compute_buffer_info_.size(); ++buf)
      {
         if (buffer == active_compute_buffer_info_[buf].buffer)
         {
            delete buffer;
            active_compute_buffer_info_.erase(active_compute_buffer_info_.begin() + buf);
            break;
         }
      }
   }
}

void
VulkanComputeDevice::BindBuffer(const VulkanComputeBuffer *buffer,
                                uint32_t                   access_flags,
                                uint32_t                   index)
{
   if (nullptr != buffer)
   {
      for (uint buf = 0; buf < active_compute_buffer_info_.size(); ++buf)
      {
         if (active_compute_buffer_info_[buf].bound &&
             active_compute_buffer_info_[buf].binding_index == index)
         {
            std::string warning_message = "Another buffer ";
            warning_message += active_compute_buffer_info_[buf].buffer->Name();
            warning_message += " (";
            std::stringstream str_stream;
            str_stream << active_compute_buffer_info_[buf].buffer;
            warning_message += str_stream.str();
            warning_message += ") is already bound to index ";
            warning_message += std::to_string(index);
            warning_message += " being requested for buffer ";
            warning_message += buffer->Name();
            warning_message += " (";
            std::stringstream str_stream2;
            str_stream2 << buffer;
            warning_message += str_stream2.str();
            warning_message += ").  Perhaps unbind the previous one first.";
            LogWarning(warning_message);
         }
      }
      for (uint buf = 0; buf < active_compute_buffer_info_.size(); ++buf)
      {
         if (buffer == active_compute_buffer_info_[buf].buffer)
         {
            active_compute_buffer_info_[buf].buffer->SetCurrentDescriptorIndex(index);
            active_compute_buffer_info_[buf].access_flags  = access_flags;
            active_compute_buffer_info_[buf].bound         = true;
            active_compute_buffer_info_[buf].binding_index = index;
            break;
         }
      }
   }
}

void
VulkanComputeDevice::UnbindBuffer(const VulkanComputeBuffer *buffer)
{
   if (nullptr != buffer)
   {
      for (uint buf = 0; buf < active_compute_buffer_info_.size(); ++buf)
      {
         if (buffer == active_compute_buffer_info_[buf].buffer)
         {
            if (!active_compute_buffer_info_[buf].bound)
            {
               std::string warning_message = "VulkanComputeDevice::UnbindBuffer called on buffer ";
               warning_message += active_compute_buffer_info_[buf].buffer->Name();
               warning_message += " (";
               std::stringstream str_stream;
               str_stream << active_compute_buffer_info_[buf].buffer;
               warning_message += str_stream.str();
               warning_message += ") that is not bound.  Ignoring.";
               LogWarning(warning_message);
            }
            else
            {
               active_compute_buffer_info_[buf].bound         = false;
               active_compute_buffer_info_[buf].binding_index = 0;
               active_compute_buffer_info_[buf].access_flags  = 0;
            }
            break;
         }
      }
   }
}

bool
VulkanComputeDevice::BeginCommandBuffer(VkCommandBuffer &vk_command_buffer)
{
   VkCommandBufferAllocateInfo cmd_buf_alloc_info = {};
   cmd_buf_alloc_info.sType                       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
   cmd_buf_alloc_info.commandPool                 = vulkan_command_pool_;
   cmd_buf_alloc_info.level                       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
   cmd_buf_alloc_info.commandBufferCount          = 1;
   if (VK_SUCCESS !=
       vkAllocateCommandBuffers(vulkan_device_, &cmd_buf_alloc_info, &vk_command_buffer))
   {
      LogError("VulkanComputeDevice::BeginCommandBuffer - Failed to allocate "
               "command buffer");
      return false;
   }

   VkCommandBufferBeginInfo cmd_buf_begin_info = {};
   cmd_buf_begin_info.sType                    = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
   cmd_buf_begin_info.flags                    = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
   cmd_buf_begin_info.pInheritanceInfo         = nullptr;
   if (VK_SUCCESS != vkBeginCommandBuffer(vk_command_buffer, &cmd_buf_begin_info))
   {
      LogError("VulkanComputeDevice::BeginCommandBuffer - Failed to begin "
               "command buffer");
      return false;
   }

   return true;
}

bool
VulkanComputeDevice::EndCommandBuffer(VkCommandBuffer &vk_command_buffer,
                                      uint32_t         x_work_size,
                                      uint32_t         y_work_size,
                                      uint32_t         z_work_size)
{
   vkCmdDispatch(vk_command_buffer, x_work_size, y_work_size, z_work_size);

   if (VK_SUCCESS != vkEndCommandBuffer(vk_command_buffer))
   {
      LogError("VulkanComputeDevice::EndCommandBuffer - Failed to end command buffer");
      return false;
   }
   return true;
}

bool
VulkanComputeDevice::SubmitCommandBuffer(VkCommandBuffer vk_command_buffer,
                                         VkSemaphore    *wait_semaphore,
                                         VkSemaphore    *signal_semaphore,
                                         VkFence         fence)
{
   VkSubmitInfo submit_info                  = {};
   submit_info.sType                         = VK_STRUCTURE_TYPE_SUBMIT_INFO;
   submit_info.commandBufferCount            = 1;
   submit_info.pCommandBuffers               = &vk_command_buffer;
   VkPipelineStageFlags pipeline_stage_flags = 0;
   if (wait_semaphore != nullptr)
   {
      submit_info.waitSemaphoreCount = 1;
      submit_info.pWaitSemaphores    = wait_semaphore;
      pipeline_stage_flags           = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
      submit_info.pWaitDstStageMask  = &pipeline_stage_flags;
   }
   if (signal_semaphore != nullptr)
   {
      submit_info.signalSemaphoreCount = 1;
      submit_info.pSignalSemaphores    = signal_semaphore;
   }

   if (VK_SUCCESS != vkQueueSubmit(vulkan_queue_, 1, &submit_info, fence))
   {
      LogError("VulkanComputeDevice::SubmitCommandBuffer - Failed to submit queue");
      return false;
   }
   return true;
}

void
VulkanComputeDevice::ReleaseCommandBuffer(VkCommandBuffer &vk_command_buffer)
{
   vkFreeCommandBuffers(vulkan_device_, vulkan_command_pool_, 1, &vk_command_buffer);
}

bool
VulkanComputeDevice::WaitForSubmissionCompletion(VkFence fence)
{
   VkResult result;
   if (fence != VK_NULL_HANDLE)
   {
      result =
        vkWaitForFences(vulkan_device_, 1, &fence, VK_TRUE, std::numeric_limits<uint32_t>::max());
      if (VK_SUCCESS != result)
      {
         LogError("VulkanComputeDevice::WaitForSubmissionComplete - Failed to wait for fence");
         return false;
      }
   }
   else
   {
      result = vkQueueWaitIdle(vulkan_queue_);
      if (VK_SUCCESS != result)
      {
         LogError("VulkanComputeDevice::WaitForSubmissionComplete - Failed to wait for queue idle");
         return false;
      }
   }
   return true;
}

bool
VulkanComputeDevice::CommandPipelineBarrier2(VkCommandBuffer         command_buffer,
                                             const VkDependencyInfo *dependency_info) const
{
   if (pfnVkCmdPipelineBarrier2_ == nullptr)
   {
      return false;
   }
   pfnVkCmdPipelineBarrier2_(command_buffer, dependency_info);
   return true;
}

bool
VulkanComputeDevice::SetObjectName(VkObjectType       object_type,
                                   uint64_t           handle,
                                   const std::string &name) const
{
   if (nullptr != pfnVkSetDebugUtilsObjectNameEXT_)
   {
      VkDebugUtilsObjectNameInfoEXT object_info = {
        VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT,
        nullptr,
        object_type,
        handle,
        name.c_str()};
      return (pfnVkSetDebugUtilsObjectNameEXT_(vulkan_device_, &object_info) == VK_SUCCESS);
   }
   return false;
}
