//
// Copyright (c) Mark Young, 2018-2020,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#pragma once

#include "vulkan_compute_device.hpp"

#include <vector>

class VulkanComputeShader
{
 public:
   class Owner
   {
    public:
      virtual const VulkanComputeDevice *GetComputeDevice(void)                             = 0;
      virtual int64_t                    GenerateHashFromString(const std::string &s) const = 0;
      virtual int64_t GenerateHashFromIntVector(const std::vector<uint32_t> &int_vec) const = 0;
   };

   VulkanComputeShader(Owner *owner);
   ~VulkanComputeShader();

   bool    LoadFromSpirvFile(const char *spirv_file_name);
   void    FreeVkShaderModule(void);
   int64_t Hash(void)
   {
      return shader_hash_;
   }

   VkPipelineShaderStageCreateInfo GetPipelineShaderStageInfo();
   VkShaderModule                  GetVkShaderModule()
   {
      return vulkan_shader_module_;
   }

 private:
   Owner                *_owner;
   std::vector<uint32_t> _spirv;
   VkShaderModule        vulkan_shader_module_;
   int64_t               shader_hash_;
};
