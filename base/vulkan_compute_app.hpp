//
// Copyright (c) Mark Young, 2021,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#pragma once

#include "vulkan_compute_buffer.hpp"
#include "vulkan_compute_device.hpp"
#include "vulkan_compute_log.hpp"
#include "vulkan_compute_pipeline.hpp"
#include "vulkan_compute_shader.hpp"
#include "vulkan_compute_shader_library.hpp"

#include <cstring>

class VulkanComputeApp
{
 public:
   VulkanComputeApp(const std::string &app_name);
   virtual ~VulkanComputeApp();

   VulkanComputeBuffer *LoadImageIntoBuffer(const std::string &image_file,
                                            int32_t           &width,
                                            int32_t           &height,
                                            int32_t           &comps,
                                            uint8_t           &comp_size);
   bool                 SaveImageFromBuffer(VulkanComputeBuffer *image_buffer,
                                            const std::string   &image_file,
                                            const int32_t        width,
                                            const int32_t        height,
                                            const int32_t        buffer_comps,
                                            const int32_t        image_comps,
                                            uint8_t              comp_size);
   VulkanComputeBuffer *CreateBlankBuffer(const std::string &name, uint64_t data_size);
   VulkanComputeBuffer *CreateRandomBuffer(const std::string &name, uint64_t data_size);

   VulkanComputePipeline *CreateComputePipeline(const std::string    &pipeline_name,
                                                const std::string    &shader_file,
                                                VulkanComputeShader **compute_shader,
                                                const char           *shader_main_name,
                                                uint64_t              push_constant_size);
   void                   FreePipeline(VulkanComputePipeline *pipeline);
   bool                   UploadPipeline(VulkanComputePipeline *pipeline);
   bool                   UnloadPipeline(VulkanComputePipeline *pipeline);
   bool BindPipeline(VulkanComputePipeline *pipeline, VkCommandBuffer command_buffer);

   bool SetInputBuffer(VulkanComputePipeline *pipeline,
                       uint32_t               index,
                       VulkanComputeBuffer   *buffer);
   bool SetOutputBuffer(VulkanComputePipeline *pipeline,
                        uint32_t               index,
                        VulkanComputeBuffer   *buffer);
   bool CreateFence(VkFence &fence);
   void DestroyFence(VkFence fence);
   bool CreateSemaphore(VkSemaphore &semaphore);
   void DestroySemaphore(VkSemaphore semaphore);

   bool StartRecordingCommandBuffer(const std::string &name, VkCommandBuffer &command_buffer);
   bool StopRecordingCommandBuffer(VkCommandBuffer command_buffer,
                                   uint32_t        x_work_size,
                                   uint32_t        y_work_size,
                                   uint32_t        z_work_size);
   bool SubmitCommandBuffer(VkCommandBuffer command_buffer,
                            VkSemaphore    *wait_semaphore,
                            VkSemaphore    *signal_semaphore,
                            VkFence         fence);
   bool WaitOnCommandBuffer(VkCommandBuffer command_buffer, VkFence fence);
   void FreeCommandBuffer(VkCommandBuffer command_buffer);

   VulkanComputeDevice *GetComputeDevice()
   {
      return compute_device_;
   }
   VkDevice GetVkDev()
   {
      return compute_device_->GetVkDevice();
   }

 private:
   bool LoadShader(const std::string &shader_file, VulkanComputeShader **compute_shader);

   VulkanComputeDevice *compute_device_;
   VkCommandBuffer      vulkan_command_buffer_;
   std::vector<VkFence> vulkan_fences_;
};
