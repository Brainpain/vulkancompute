//
// Copyright (c) Mark Young, 2021,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "vulkan_compute_app.hpp"

#include <cstring>

#define STBI_HEADER_FILE_ONLY 1
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h" // http://nothings.org/stb_image.c

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h" // http://nothings.org/stb/stb_image_write.h

VulkanComputeApp::VulkanComputeApp(const std::string &app_name)
{
   compute_device_ = new VulkanComputeDevice(app_name);
   if (compute_device_ == nullptr)
   {
      printf("Error: Failed to create compute device!\n");
      exit(-1);
   }
}

VulkanComputeApp::~VulkanComputeApp()
{
   delete compute_device_;
   compute_device_ = nullptr;
}

VulkanComputeBuffer *
VulkanComputeApp::LoadImageIntoBuffer(const std::string &image_file,
                                      int32_t           &width,
                                      int32_t           &height,
                                      int32_t           &comps,
                                      uint8_t           &comp_size)
{
   std::string    image_file_name = "resources/images/" + image_file;
   unsigned char *image_read_data =
     stbi_load(image_file_name.c_str(), &width, &height, &comps, STBI_rgb);
   if (nullptr == image_read_data || width == 0 || height == 0 || comps == 0)
   {
      LogFatalError("Failed to read in image file");
      return nullptr;
   }

   // For shaders, use 1 uint for each component.
   uint32_t             num_incoming_pixel_comps = width * height * comps;
   uint32_t             num_outgoing_pixel_comps = width * height * 4;
   uint64_t             outgoing_data_size       = num_outgoing_pixel_comps * sizeof(uint32_t);
   VulkanComputeBuffer *image_buffer =
     compute_device_->CreateBuffer(image_file.c_str(), outgoing_data_size);
   if (nullptr == image_buffer)
   {
      LogFatalError("Failed to create image buffer");
      return nullptr;
   }

   // Fill in the image buffer with the image data
   uint32_t *image_buffer_mem =
     reinterpret_cast<uint32_t *>(image_buffer->MapMemoryPointer(outgoing_data_size, 0));
   uint32_t outgo;
   uint32_t incom;
   for (outgo = 0, incom = 0; incom < num_incoming_pixel_comps;)
   {
      image_buffer_mem[outgo++] = image_read_data[incom++];
      if ((incom % 3) == 0)
      {
         image_buffer_mem[outgo++] = 0;
      }
   }
   image_buffer->UnmapMemoryPointer();

   // Free our local image memory used for reading
   stbi_image_free(image_read_data);

   LogInfo("Input image loaded");
   return image_buffer;
}

bool
VulkanComputeApp::SaveImageFromBuffer(VulkanComputeBuffer *image_buffer,
                                      const std::string   &image_file,
                                      const int32_t        width,
                                      const int32_t        height,
                                      const int32_t        buffer_comps,
                                      const int32_t        image_comps,
                                      uint8_t              comp_size)
{
   uint32_t  num_incoming_fragments = width * height * buffer_comps;
   uint32_t  num_outgoing_fragments = width * height * image_comps;
   uint64_t  incoming_data_size     = num_incoming_fragments * comp_size;
   uint64_t  outgoing_data_size     = num_outgoing_fragments;
   uint32_t *image_buffer_mem =
     reinterpret_cast<uint32_t *>(image_buffer->MapMemoryPointer(incoming_data_size, 0));
   if (nullptr == image_buffer_mem)
   {
      return false;
   }
   // Brainpinimage_buffer->FlushMemory(incoming_data_size, 0);
   uint8_t *image_data = new uint8_t[outgoing_data_size];
   if (nullptr == image_data)
   {
      return false;
   }

   // Copy data from 1 uint32_t per component to 1 uint8_t per component and
   // use that for saving the image data.
   uint32_t max_comp = (buffer_comps > image_comps) ? buffer_comps : image_comps;
   uint32_t outgo    = 0;
   uint32_t incom    = 0;
   for (uint32_t y = 0; y < height; ++y)
   {
      for (uint32_t x = 0; x < width; ++x)
      {
         for (uint32_t c = 0; c < max_comp; ++c)
         {
            if (c >= buffer_comps)
            {
               image_data[outgo++] = static_cast<uint8_t>(image_buffer_mem[incom - 1]);
            }
            else if (c < image_comps)
            {
               uint8_t value = 0;
               if (image_buffer_mem[incom] > 255)
               {
                  value = 255;
               }
               else
               {
                  value = static_cast<uint8_t>(image_buffer_mem[incom]);
               }
               image_data[outgo++] = value;
               incom++;
            }
            else
            {
               incom++;
            }
         }
      }
   }

   if (!stbi_write_jpg(image_file.c_str(), width, height, image_comps, image_data, 50))
   {
      LogFatalError("Failed to write data to image file");
      return false;
   }
   delete[] image_data;
   image_buffer->UnmapMemoryPointer();

   std::string out_image = "Output image ";
   out_image += image_file.c_str();
   out_image += " saved";
   LogInfo(out_image);
   return true;
}

VulkanComputeBuffer *
VulkanComputeApp::CreateBlankBuffer(const std::string &name, uint64_t data_size)
{
   VulkanComputeBuffer *zero_buffer = compute_device_->CreateBuffer(name, data_size);

   // Fill in the output buffer with just 0s.
   uint32_t *int_values = reinterpret_cast<uint32_t *>(zero_buffer->MapMemoryPointer(data_size, 0));
   memset(int_values, 0, data_size);
   zero_buffer->UnmapMemoryPointer();
   return zero_buffer;
}

VulkanComputeBuffer *
VulkanComputeApp::CreateRandomBuffer(const std::string &name, uint64_t data_size)
{
   VulkanComputeBuffer *rand_buffer = compute_device_->CreateBuffer(name, data_size);

   // Fill in the output buffer with just 0s.
   uint32_t *int_values = reinterpret_cast<uint32_t *>(rand_buffer->MapMemoryPointer(data_size, 0));
   for (uint64_t count = 0; count < (data_size / sizeof(int32_t)); ++count)
   {
      int_values[count] = rand();
   }
   rand_buffer->UnmapMemoryPointer();
   return rand_buffer;
}

bool
VulkanComputeApp::LoadShader(const std::string &shader_file, VulkanComputeShader **compute_shader)
{
   *compute_shader =
     compute_device_->GetComputeShaderLibrary()->LoadFromSpirvFile(shader_file.c_str());
   if (nullptr == *compute_shader)
   {
      LogFatalError("Failed to load SpirV");
      return false;
   }

   return true;
}

VulkanComputePipeline *
VulkanComputeApp::CreateComputePipeline(const std::string    &pipeline_name,
                                        const std::string    &shader_file,
                                        VulkanComputeShader **compute_shader,
                                        const char           *shader_main_name,
                                        uint64_t              push_constant_size)
{
   if (!LoadShader(shader_file, compute_shader))
   {
      return nullptr;
   }
   VulkanComputePipeline *pipeline =
     new VulkanComputePipeline(pipeline_name,
                               compute_device_,
                               (*compute_shader)->GetVkShaderModule(),
                               shader_main_name,
                               push_constant_size);
   if (pipeline == nullptr)
   {
      std::string error_message = "VulkanComputeApp::CreateComputePipeline(";
      error_message += pipeline_name;
      error_message += ") - Failed creating compute pipeline";
      LogFatalError(error_message);
   }
   return pipeline;
}

void
VulkanComputeApp::FreePipeline(VulkanComputePipeline *pipeline)
{
   delete pipeline;
}

bool
VulkanComputeApp::SetInputBuffer(VulkanComputePipeline *pipeline,
                                 uint32_t               layout_index,
                                 VulkanComputeBuffer   *buffer)
{
   if (pipeline == nullptr)
   {
      LogFatalError("VulkanComputeApp::SetInputBuffer called with nullptr pipeline");
      return false;
   }
   pipeline->BindBuffer(
     buffer, VulkanComputeBuffer::VULKAN_COMPUTE_BUFFER_ACCESS_FLAG_INPUT, layout_index);
   return true;
}

bool
VulkanComputeApp::SetOutputBuffer(VulkanComputePipeline *pipeline,
                                  uint32_t               layout_index,
                                  VulkanComputeBuffer   *buffer)
{
   if (pipeline == nullptr)
   {
      LogFatalError("VulkanComputeApp::SetOutputBuffer called with nullptr pipeline");
      return false;
   }
   pipeline->BindBuffer(
     buffer, VulkanComputeBuffer::VULKAN_COMPUTE_BUFFER_ACCESS_FLAG_OUTPUT, layout_index);
   return true;
}

bool
VulkanComputeApp::StartRecordingCommandBuffer(const std::string &name,
                                              VkCommandBuffer   &command_buffer)
{
   if (!compute_device_->BeginCommandBuffer(command_buffer))
   {
      LogError("Failed beginning command buffer");
      return false;
   }

   if (name.length() > 0)
   {
      compute_device_->SetObjectName(
        VK_OBJECT_TYPE_COMMAND_BUFFER, reinterpret_cast<uint64_t>(command_buffer), name.c_str());
   }
   return true;
}

bool
VulkanComputeApp::UploadPipeline(VulkanComputePipeline *pipeline)
{
   if (pipeline == nullptr)
   {
      LogFatalError("VulkanComputeApp::UploadPipeline called with nullptr pipeline");
      return false;
   }
   if (!pipeline->UploadToDevice())
   {
      LogError("Failed uploading pipeline");
      return false;
   }
   return true;
}

bool
VulkanComputeApp::UnloadPipeline(VulkanComputePipeline *pipeline)
{
   if (pipeline == nullptr)
   {
      LogFatalError("VulkanComputeApp::BindPipeline called with nullptr pipeline");
      return false;
   }
   if (!pipeline->UnloadFromDevice())
   {
      LogError("Failed unloading pipeline");
      return false;
   }
   return true;
}

bool
VulkanComputeApp::BindPipeline(VulkanComputePipeline *pipeline, VkCommandBuffer command_buffer)
{
   if (pipeline == nullptr)
   {
      LogFatalError("VulkanComputeApp::BindPipeline called with nullptr pipeline");
      return false;
   }
   pipeline->Bind(command_buffer);
   return true;
}

bool
VulkanComputeApp::StopRecordingCommandBuffer(VkCommandBuffer command_buffer,
                                             uint32_t        x_work_size,
                                             uint32_t        y_work_size,
                                             uint32_t        z_work_size)
{
   if (!compute_device_->EndCommandBuffer(command_buffer, x_work_size, y_work_size, z_work_size))
   {
      LogError("Failed ending command buffer");
      return false;
   }
   return true;
}

bool
VulkanComputeApp::SubmitCommandBuffer(VkCommandBuffer command_buffer,
                                      VkSemaphore    *wait_semaphore,
                                      VkSemaphore    *signal_semaphore,
                                      VkFence         fence)
{
   LogInfo("Submitting command buffer");
   if (!compute_device_->SubmitCommandBuffer(
         command_buffer, wait_semaphore, signal_semaphore, fence))
   {
      LogError("Failed submitting command buffer");
      return false;
   }
   return true;
}

bool
VulkanComputeApp::WaitOnCommandBuffer(VkCommandBuffer command_buffer, VkFence fence)
{
   if (!compute_device_->WaitForSubmissionCompletion(fence))
   {
      LogError("Failed waiting for submission to complete");
      return false;
   }
   return true;
}

bool
VulkanComputeApp::CreateFence(VkFence &fence)
{
   VkFenceCreateInfo fence_create_info = {VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, nullptr, 0};
   if (VK_SUCCESS !=
       vkCreateFence(compute_device_->GetVkDevice(), &fence_create_info, nullptr, &fence))
   {
      return false;
   }
   return true;
}

void
VulkanComputeApp::DestroyFence(VkFence fence)
{
   vkDestroyFence(compute_device_->GetVkDevice(), fence, nullptr);
}

bool
VulkanComputeApp::CreateSemaphore(VkSemaphore &semaphore)
{
   VkSemaphoreCreateInfo semaphore_create_info = {
     VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, nullptr, 0};
   if (VK_SUCCESS != vkCreateSemaphore(
                       compute_device_->GetVkDevice(), &semaphore_create_info, nullptr, &semaphore))
   {
      return false;
   }
   return true;
}

void
VulkanComputeApp::DestroySemaphore(VkSemaphore semaphore)
{
   vkDestroySemaphore(compute_device_->GetVkDevice(), semaphore, nullptr);
}

void
VulkanComputeApp::FreeCommandBuffer(VkCommandBuffer command_buffer)
{
   compute_device_->ReleaseCommandBuffer(command_buffer);
}
