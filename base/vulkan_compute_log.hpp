//
// Copyright (c) Mark Young, 2018, All rights reserved.
// SPDX-License-Identifier: MIT
//

#pragma once

#include <iostream>

inline void
LogInfo(const std::string &message)
{
   std::cout << "VulkanCompute [INFO] - " << message << std::endl;
}

inline void
LogWarning(const std::string &message)
{
   std::cout << "VulkanCompute [WARNING] - " << message << std::endl;
}

inline void
LogError(const std::string &message)
{
   std::cerr << "VulkanCompute [ERROR] - " << message << std::endl;
}

inline void
LogFatalError(const std::string &message)
{
   std::cerr << "VulkanCompute [FATAL] - " << message << std::endl;
   exit(-1);
}
