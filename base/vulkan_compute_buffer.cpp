//
// Copyright (c) Mark Young, 2018-2020,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "vulkan_compute_buffer.hpp"

#include "vulkan_compute_device.hpp"
#include "vulkan_compute_log.hpp"

#include <cstring>

VulkanComputeBuffer::VulkanComputeBuffer(const std::string   &name,
                                         VulkanComputeDevice *compute_device,
                                         size_t               size)
  : parent_device_(compute_device)
{
   uint32_t vk_compute_queue_index = compute_device->GetVkQueueFamilyIndex();
   vulkan_device_                  = compute_device->GetVkDevice();
   name_                           = name;

   if (0 == size)
   {
      std::string error_message = "Buffer ";
      error_message += name_;
      error_message += " - VulkanCompute::CreateBuffer invalid size";
      LogFatalError(error_message);
   }

   if (compute_device->GetVkDeviceProperties().limits.maxStorageBufferRange < size)
   {
      LogFatalError("Creating storage buffer larger than allowable size");
   }

   // Create the buffer
   VkBufferCreateInfo buffer_create_info    = {};
   buffer_create_info.sType                 = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
   buffer_create_info.flags                 = 0;
   buffer_create_info.size                  = size;
   buffer_create_info.usage                 = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
   buffer_create_info.sharingMode           = VK_SHARING_MODE_EXCLUSIVE;
   buffer_create_info.queueFamilyIndexCount = 1;
   buffer_create_info.pQueueFamilyIndices   = &vk_compute_queue_index;
   if (VK_SUCCESS != vkCreateBuffer(vulkan_device_, &buffer_create_info, nullptr, &vulkan_buffer_))
   {
      std::string error_message = "Buffer ";
      error_message += name_;
      error_message += " - VulkanCompute::CreateBuffer failed to create Vulkan buffer!";
      LogFatalError(error_message);
   }

   vkGetBufferMemoryRequirements(
     vulkan_device_, vulkan_buffer_, &vulkan_buffer_memory_requirements_);

   size_t alignment = vulkan_buffer_memory_requirements_.alignment;
   size_t alignment_adjust = alignment - 1;
   size_t total_required_size =
     (vulkan_buffer_memory_requirements_.size + alignment_adjust) & ~alignment_adjust;

   // Look for memory that is both host visible and coherent
   uint32_t memory_type_index = 0;
   if (!SelectMemoryType(
         compute_device->GetVkMemoryProperties(),
         total_required_size,
         (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
         memory_type_index))
   {
      is_coherent_ = false;
      if (!SelectMemoryType(compute_device->GetVkMemoryProperties(),
                            total_required_size,
                            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
                            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                            memory_type_index))
      {
         std::string error_message = "Buffer ";
         error_message += name_;
         error_message += " - VulkanCompute::CreateBuffer failed to select memory type!";
         LogFatalError(error_message);
      }
   }
   else
   {
      is_coherent_ = true;
   }
   memory_size_      = total_required_size;
   memory_alignment_ = vulkan_buffer_memory_requirements_.alignment;

   // Allocate the buffer memory from the selected memory type
   VkMemoryAllocateInfo memory_alloc_info = {};
   memory_alloc_info.sType                = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
   memory_alloc_info.allocationSize       = total_required_size;
   memory_alloc_info.memoryTypeIndex      = memory_type_index;
   if (VK_SUCCESS !=
       vkAllocateMemory(vulkan_device_, &memory_alloc_info, nullptr, &vulkan_device_memory_))
   {
      std::string error_message = "Buffer ";
      error_message += name_;
      error_message += " - VulkanCompute::CreateBuffer failed to allocate memory from type!";
      LogFatalError(error_message);
   }

   // Bind the memory to the buffer we just created
   vkBindBufferMemory(vulkan_device_, vulkan_buffer_, vulkan_device_memory_, 0);
}

VulkanComputeBuffer::~VulkanComputeBuffer()
{
   vkFreeMemory(vulkan_device_, vulkan_device_memory_, nullptr);
   vkDestroyBuffer(vulkan_device_, vulkan_buffer_, nullptr);
}

bool
VulkanComputeBuffer::SelectMemoryType(VkPhysicalDeviceMemoryProperties &memory_props,
                                      size_t                            size,
                                      uint32_t                          required_flags,
                                      uint32_t                          desired_flags,
                                      uint32_t                         &memory_type_index)
{
   // Look for memory that is both host visible and coherent
   int32_t desired_index = -1;
   for (int32_t type_index = 0; type_index < static_cast<int32_t>(memory_props.memoryTypeCount);
        ++type_index)
   {
      // Make sure the memory type at least matches the required flags and has
      // enough size for us to create the buffer we need.
      if ((memory_props.memoryTypes[type_index].propertyFlags & required_flags) == required_flags &&
          memory_props.memoryHeaps[memory_props.memoryTypes[type_index].heapIndex].size >= size)
      {
         desired_index = type_index;

         // If this is device local, host visible and coherent, then we've struck
         // Gold so quit.
         if (0 != (memory_props.memoryTypes[type_index].propertyFlags & desired_flags))
         {
            break;
         }
      }
   }
   if (desired_index >= 0)
   {
      memory_type_index = static_cast<uint32_t>(desired_index);

      // Now remove the memory of this buffer from the list so we can make sure we
      // don't allocate too much.
      memory_props.memoryHeaps[memory_props.memoryTypes[memory_type_index].heapIndex].size -= size;
      return true;
   }
   return false;
}

bool
VulkanComputeBuffer::WriteData(size_t size, size_t offset, const uint8_t *data)
{
   if (memory_size_ < size + offset)
   {
      std::string error_message = "Buffer ";
      error_message += name_;
      error_message += " - VulkanComputeBuffer::WriteData called to write "
                       "outside of memory.  Offset + size = ";
      error_message += std::to_string(size + offset);
      error_message += " bytes, but memory is only ";
      error_message += std::to_string(memory_size_);
      error_message += " bytes.";
      LogError(error_message);
      return false;
   }
   uint8_t *memory = MapMemoryPointer(size, offset);
   if (memory != nullptr)
   {
      memcpy(memory, data, size);
      // BrainpainFlushMemory(size, offset);
      UnmapMemoryPointer();
      return true;
   }
   else
   {
      return false;
   }
}

uint8_t *
VulkanComputeBuffer::MapMemoryPointer(size_t size, size_t offset)
{
   if ((offset & memory_alignment_ - 1) != 0)
   {
      std::string error_message = "Buffer ";
      error_message += name_;
      error_message += " - VulkanComputeBuffer::MapMemoryPointer called with offset ";
      error_message += std::to_string(offset);
      error_message += ", but memory alignment is ";
      error_message += std::to_string(memory_alignment_);
      error_message += ".";
      LogError(error_message);
      return nullptr;
   }
   if (memory_size_ < size + offset)
   {
      std::string error_message = "Buffer ";
      error_message += name_;
      error_message += " - VulkanComputeBuffer::MapMemoryPointer called to write "
                       "outside of memory.  Offset + size = ";
      error_message += std::to_string(size + offset);
      error_message += " bytes, but memory is only ";
      error_message += std::to_string(memory_size_);
      error_message += " bytes.";
      LogError(error_message);
      return nullptr;
   }
   uint8_t *data_pointer = nullptr;
   VkResult map_result   = vkMapMemory(vulkan_device_,
                                     vulkan_device_memory_,
                                     offset,
                                     size,
                                     0,
                                     reinterpret_cast<void **>(&data_pointer));
   if (VK_SUCCESS != map_result)
   {
      std::string error_message = "Buffer ";
      error_message += name_;
      error_message += " - VulkanComputeBuffer::MapMemoryPointer failed Vulkan "
                       "vkMapMemory command with result ";
      error_message += std::to_string(map_result);
      error_message += ".";
      LogError(error_message);
      return nullptr;
   }
   is_mapped_ = true;
   return data_pointer;
}

void
VulkanComputeBuffer::UnmapMemoryPointer()
{
   if (is_mapped_)
   {
      vkUnmapMemory(vulkan_device_, vulkan_device_memory_);
      is_mapped_ = false;
   }
}

void
VulkanComputeBuffer::FlushMemory(size_t size, size_t offset)
{
   const VkPhysicalDeviceLimits &limits = parent_device_->GetVkDeviceProperties().limits;

   size_t adjusted_size =
     (size + (limits.nonCoherentAtomSize - 1)) & ~(limits.nonCoherentAtomSize - 1);
   if (memory_size_ < adjusted_size + offset)
   {
      std::string error_message = "Buffer ";
      error_message += name_;
      error_message += " - VulkanComputeBuffer::FlushMemory called to flush "
                       "outside of memory.  Offset + size = ";
      error_message += std::to_string(adjusted_size + offset);
      error_message += " bytes, but memory is only ";
      error_message += std::to_string(memory_size_);
      error_message += " bytes.";
      LogError(error_message);
   }

   VkMappedMemoryRange flush_range = {};
   flush_range.sType               = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
   flush_range.memory              = vulkan_device_memory_;

   if (memory_size_ <= adjusted_size + offset)
   {
      flush_range.size   = VK_WHOLE_SIZE;
      flush_range.offset = 0;
   }
   else
   {
      flush_range.size   = adjusted_size;
      flush_range.offset = offset;
   }

   VkResult flush_result = vkFlushMappedMemoryRanges(vulkan_device_, 1, &flush_range);
   if (VK_SUCCESS != flush_result)
   {
      std::string error_message = "Buffer ";
      error_message += name_;
      error_message += " - VulkanComputeBuffer::FlushMemory failed Vulkan "
                       "vkFlushMappedMemoryRanges command with result ";
      error_message += std::to_string(flush_result);
      error_message += ".";
      LogError(error_message);
   }
}

VkDescriptorSetLayoutBinding
VulkanComputeBuffer::GetVkDescriptorSetLayout() const
{
   VkDescriptorSetLayoutBinding descriptorSetLayoutBinding = {};
   descriptorSetLayoutBinding.binding                      = vulkan_descriptor_index_;
   descriptorSetLayoutBinding.descriptorType               = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
   descriptorSetLayoutBinding.descriptorCount              = 1;
   descriptorSetLayoutBinding.stageFlags                   = VK_SHADER_STAGE_COMPUTE_BIT;
   return descriptorSetLayoutBinding;
}

VkDescriptorBufferInfo
VulkanComputeBuffer::GetVkDescriptorBufferInfo() const
{
   VkDescriptorBufferInfo buffer_info = {};
   buffer_info.buffer                 = vulkan_buffer_;
   buffer_info.offset                 = 0;
   buffer_info.range                  = VK_WHOLE_SIZE;
   return buffer_info;
}

VkWriteDescriptorSet
VulkanComputeBuffer::GetVkWriteDescriptorSet(VkDescriptorSet         vulkan_desc_set,
                                             VkDescriptorBufferInfo *vulkan_desc_buf_info) const
{
   VkWriteDescriptorSet write_desc_set = {};
   write_desc_set.sType                = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
   write_desc_set.dstSet               = vulkan_desc_set;
   write_desc_set.dstBinding           = vulkan_descriptor_index_;
   write_desc_set.dstArrayElement      = 0;
   write_desc_set.descriptorCount      = 1;
   write_desc_set.descriptorType       = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
   write_desc_set.pBufferInfo          = vulkan_desc_buf_info;
   return write_desc_set;
}

void
VulkanComputeBuffer::TransitionFromOutputToInput(VkCommandBuffer command_buffer)
{
   if (parent_device_->SupportsSync2())
   {
      VkMemoryBarrier2 memory_barrier = {VK_STRUCTURE_TYPE_MEMORY_BARRIER_2,
                                         nullptr,
                                         VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT_KHR,
                                         VK_ACCESS_2_SHADER_WRITE_BIT_KHR,
                                         VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT_KHR,
                                         VK_ACCESS_2_SHADER_READ_BIT_KHR};

      VkDependencyInfo dependency_info = {
        VK_STRUCTURE_TYPE_DEPENDENCY_INFO, nullptr, 0, 1, &memory_barrier, 0, nullptr, 0, nullptr};
      parent_device_->CommandPipelineBarrier2(command_buffer, &dependency_info);
   }
   else
   {
      VkMemoryBarrier memory = {VK_STRUCTURE_TYPE_MEMORY_BARRIER,
                                nullptr,
                                VK_ACCESS_2_SHADER_WRITE_BIT_KHR,
                                VK_ACCESS_2_SHADER_READ_BIT_KHR};
      vkCmdPipelineBarrier(command_buffer,
                           VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                           VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                           0,
                           1,
                           &memory,
                           0,
                           nullptr,
                           0,
                           nullptr);
   }
}
