//
// Copyright (c) Mark Young, 2018-2020,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#pragma once

#include "vulkan_compute_buffer.hpp"

#include <string>
#include <vector>

class VulkanComputeShaderLibrary;

class VulkanComputeDevice
{
 public:
   VulkanComputeDevice(const std::string &application_name);
   ~VulkanComputeDevice();

   VkPhysicalDeviceMemoryProperties &GetVkMemoryProperties()
   {
      return vulkan_memory_properties_;
   }
   VkDevice GetVkDevice() const
   {
      return vulkan_device_;
   }
   void GetDeviceVersion(uint8_t &major, uint8_t &minor) const
   {
      major = major_version_;
      minor = minor_version_;
   }
   uint32_t GetVkQueueFamilyIndex() const
   {
      return vulkan_queue_family_index_;
   }
   VulkanComputeShaderLibrary *GetComputeShaderLibrary()
   {
      return shader_library_;
   }
   const VkPhysicalDeviceProperties &GetVkDeviceProperties() const
   {
      return vulkan_device_properties_;
   };

   VulkanComputeBuffer *CreateBuffer(const std::string &name, size_t size);
   void                 DestroyBuffer(VulkanComputeBuffer *buffer);
   void BindBuffer(const VulkanComputeBuffer *buffer, uint32_t access_flags, uint32_t index);
   void UnbindBuffer(const VulkanComputeBuffer *buffer);

   bool BeginCommandBuffer(VkCommandBuffer &vk_command_buffer);
   bool EndCommandBuffer(VkCommandBuffer &vk_command_buffer,
                         uint32_t         x_work_size,
                         uint32_t         y_work_size,
                         uint32_t         z_work_size);
   bool SubmitCommandBuffer(VkCommandBuffer vk_command_buffer,
                            VkSemaphore    *wait_semaphore,
                            VkSemaphore    *signal_semaphore,
                            VkFence         fence);
   bool WaitForSubmissionCompletion(VkFence fence);
   void ReleaseCommandBuffer(VkCommandBuffer &vk_command_buffer);
   bool CommandPipelineBarrier2(VkCommandBuffer         command_buffer,
                                const VkDependencyInfo *dependency_info) const;
   bool SetObjectName(VkObjectType object_type, uint64_t handle, const std::string &name) const;

   bool SupportsSync2(void) const
   {
      return supports_sync2_;
   }

 private:
   struct ComputeBufferInfo
   {
      VulkanComputeBuffer *buffer;
      bool                 bound;
      uint32_t             binding_index;
      uint32_t             access_flags;
   };
   void InitAndSelectDevice(const std::string &application_name);
   bool DebugUtilsPresent();
   bool ValidationLayerPresent();

   uint8_t                          major_version_;
   uint8_t                          minor_version_;
   VkInstance                       vulkan_instance_;
   VkPhysicalDevice                 vulkan_physical_device_;
   VkPhysicalDeviceProperties       vulkan_device_properties_;
   VkPhysicalDeviceMemoryProperties vulkan_memory_properties_;
   VkDevice                         vulkan_device_;
   bool                             supports_sync2_ {false};
   bool                             supports_shader_float64_ {false};
   uint32_t                         vulkan_queue_family_index_;
   VkQueue                          vulkan_queue_;
   VkCommandPool                    vulkan_command_pool_;
   uint32_t                         max_work_group_counts_[3];
   std::vector<ComputeBufferInfo>   active_compute_buffer_info_;
   VulkanComputeShaderLibrary      *shader_library_;

   // Debug Utils items
   VkDebugUtilsMessengerEXT            vulkan_debug_utils_messenger_;
   PFN_vkDestroyDebugUtilsMessengerEXT pfnVkDestroyDebugUtilsMessengerEXT_;
   PFN_vkSetDebugUtilsObjectNameEXT    pfnVkSetDebugUtilsObjectNameEXT_;
   PFN_vkCmdPipelineBarrier2           pfnVkCmdPipelineBarrier2_;
};