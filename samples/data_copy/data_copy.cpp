//
// Copyright (c) Mark Young, 2018-2020,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "vulkan_compute_app.hpp"

int
main(int argc, const char **argv)
{
   VulkanComputeApp       compute_app("Data Copy Sample");
   uint32_t               num_ints       = 16384;
   const uint64_t         data_size      = num_ints * sizeof(int32_t);
   VulkanComputeShader   *compute_shader = nullptr;
   VulkanComputePipeline *compute_pipeline;

   VulkanComputeBuffer *input_buffer  = compute_app.CreateRandomBuffer("input_buffer", data_size);
   VulkanComputeBuffer *output_buffer = compute_app.CreateBlankBuffer("output_buffer", data_size);

   // Make sure data does not match
   bool     matches = true;
   int32_t *input_values =
     reinterpret_cast<int32_t *>(input_buffer->MapMemoryPointer(data_size, 0));
   int32_t *output_values =
     reinterpret_cast<int32_t *>(output_buffer->MapMemoryPointer(data_size, 0));
   for (uint32_t count = 0; count < num_ints; ++count)
   {
      if (input_values[count] != output_values[count])
      {
         matches = false;
      }
   }
   if (matches)
   {
      LogError("Input/Output data matches during init!");
   }
   else
   {
      LogInfo("Verified data does not match during init.");
   }

   input_buffer->UnmapMemoryPointer();
   output_buffer->UnmapMemoryPointer();

   compute_pipeline = compute_app.CreateComputePipeline(
     "main_pipeline", "data_copy.spv", &compute_shader, "main", 0);
   if (compute_pipeline == nullptr)
   {
      LogFatalError("Failed to create compute pipeline.");
   }

   if (!compute_app.SetInputBuffer(compute_pipeline, 0, input_buffer) ||
       !compute_app.SetOutputBuffer(compute_pipeline, 1, output_buffer))
   {
      LogFatalError("Failed to setting input/output buffers");
   }

   if (!compute_app.UploadPipeline(compute_pipeline))
   {
      LogFatalError("Failed uploading pipeline");
   }

   VkCommandBuffer command_buffer;
   if (!compute_app.StartRecordingCommandBuffer("copy cmdbuf", command_buffer))
   {
      LogFatalError("Failed starting command buffer");
   }
   if (!compute_app.BindPipeline(compute_pipeline, command_buffer))
   {
      LogFatalError("Failed binding pipeline");
   }
   if (!compute_app.StopRecordingCommandBuffer(command_buffer, num_ints, 1, 1))
   {
      LogFatalError("Failed stopping command buffer");
   }
   if (!compute_app.SubmitCommandBuffer(command_buffer, nullptr, nullptr, VK_NULL_HANDLE))
   {
      LogFatalError("Failed submitting command buffer contents");
   }
   if (!compute_app.WaitOnCommandBuffer(command_buffer, VK_NULL_HANDLE))
   {
      LogFatalError("Failed waiting on command buffer");
   }

   // Input and output queues should now match
   matches       = true;
   input_values  = reinterpret_cast<int32_t *>(input_buffer->MapMemoryPointer(data_size, 0));
   output_values = reinterpret_cast<int32_t *>(output_buffer->MapMemoryPointer(data_size, 0));
   for (uint32_t count = 0; count < num_ints; ++count)
   {
      if (input_values[count] != output_values[count])
      {
         std::string error_message = "Input/Output do not match at index ";
         error_message += std::to_string(count);
         error_message += ".  Input = ";
         error_message += std::to_string(input_values[count]);
         error_message += ", Output = ";
         error_message += std::to_string(output_values[count]);
         error_message += ".\n";
         LogError(error_message);
         matches = false;
      }
   }
   if (matches)
   {
      LogInfo("Content Matches!  Copy occurred properly!");
   }
   input_buffer->UnmapMemoryPointer();
   output_buffer->UnmapMemoryPointer();

   compute_app.UnloadPipeline(compute_pipeline);
   compute_app.FreeCommandBuffer(command_buffer);
   compute_app.FreePipeline(compute_pipeline);

   LogInfo("Succeeded to end!");
   return 0;
}
