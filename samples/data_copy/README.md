# Data Copy Sample

This is the simplest and first sample and uses an input buffer and an output
buffer and uses the compute shader to copy the contents of the input buffer into
the output buffer using the GPU.

The test creates a buffer with random data, then creates a blank buffer.
After that it compares that values to make sure the buffers are different.
The test then executes the commands to execute the copy on the GPU.
Finally, to verify the results are copied over properly, the original and new
buffer are compared again.

### Uses
 - Storage buffer for input
 - Storage buffer for output

## Resources

### Shader

The shader data_copy.spv is generated at compile time from the GLSL shader file
[data_copy.glsl](../../resources/shaders/data_copy.glsl) using Glslang.

### Source

The source file [data_copy.cpp](./data_copy.cpp) is found in the current directory.

## Data_Copy Expected Output

```
VulkanCompute [INFO] - InitAndSelectDevice: DebugUtilsPresent
VulkanCompute [INFO] - Verified data does not match during init.
VulkanCompute [INFO] - Submitting command buffer
VulkanCompute [INFO] - Content Matches!  Copy occurred properly!
VulkanCompute [INFO] - Succeeded to end!
```

