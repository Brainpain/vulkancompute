# Gaussian Sample

This sample performs a Gaussian blur using an input image.
It generates the Gaussian kernel on the CPU and passes it to the GPU using
another input storage buffer.

### Uses
 - Push constants
 - Storage buffer for input image
 - Storage buffer for gaussian kernel as an array
 - Storage buffer for output image

## Resources

### Shader

The shader gaussian.spv is generated at compile time from the GLSL shader file
[gaussian.glsl](../../resources/shaders/gaussian.glsl) using Glslang.

### Source

The source file [gaussian.cpp](./gaussian.cpp) is found in the current directory.

### Input Image

* [us_and_dogs.jpeg](../../resources/images/us_and_dogs.jpeg)

## Gaussian Expected Output

**Console:**

```
VulkanCompute [INFO] - InitAndSelectDevice: DebugUtilsPresent
VulkanCompute [INFO] - Input image loaded
VulkanCompute [INFO] - Submitting command buffer
VulkanCompute [INFO] - Output image saved
VulkanCompute [INFO] - Succeeded to end!
```

**Generate Images:**

A new image will be created called `guassian.jpeg` that is a blurred version of
the input image.

