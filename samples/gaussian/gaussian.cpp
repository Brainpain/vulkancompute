//
// Copyright (c) Mark Young, 2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "utility.hpp"
#include "vulkan_compute_app.hpp"

struct PushConstants
{
   struct
   {
      uint32_t x;
      uint32_t y;
      uint32_t z;
      uint32_t w;
   } data_vec;
   int32_t gaussian_kernel_width;
};

int
main(int argc, const char **argv)
{
   int32_t                image_width;
   int32_t                image_height;
   int32_t                image_comps;
   int32_t                buffer_comps = 4;
   uint8_t                comp_size;
   const int32_t          workgroup_size = 16; // NOTE: This number MUST match the shader's
   int32_t                kernel_width   = 151;
   float                  kernel_sigma   = float(kernel_width / 7);
   VulkanComputeApp       gaussian_app("Gaussian Sample");
   VulkanComputeShader   *compute_shader;
   std::vector<float>     gaussian_kernel;
   VkFence                gaussian_fence;
   VulkanComputePipeline *compute_pipeline;

   if (kernel_sigma < 1.f)
   {
      kernel_sigma = 1.f;
   }

   if (!vulkan_compute ::utility::GenerateGaussianKernel(
         kernel_width, kernel_sigma, gaussian_kernel))
   {
      LogFatalError("Failed to create gaussian kernel.");
   }

   VulkanComputeBuffer *input_image_buffer = gaussian_app.LoadImageIntoBuffer(
     "dog_sledding.jpeg", image_width, image_height, image_comps, comp_size);
   VulkanComputeBuffer *output_image_buffer = gaussian_app.CreateBlankBuffer(
     "output_image",
     static_cast<uint64_t>(image_width * image_height * buffer_comps * sizeof(uint32_t)));

   size_t               gaussian_kernel_size = gaussian_kernel.size() * sizeof(float);
   VulkanComputeBuffer *input_gaussian_buffer =
     gaussian_app.CreateBlankBuffer("gaussian_kernel", gaussian_kernel_size);
   int32_t *input_values =
     reinterpret_cast<int32_t *>(input_gaussian_buffer->MapMemoryPointer(gaussian_kernel_size, 0));
   memcpy(input_values, gaussian_kernel.data(), gaussian_kernel_size);
   input_gaussian_buffer->UnmapMemoryPointer();

   compute_pipeline = gaussian_app.CreateComputePipeline(
     "main_pipeline", "gaussian.spv", &compute_shader, "main", sizeof(PushConstants));
   if (compute_pipeline == nullptr)
   {
      LogFatalError("Failed to create compute pipeline.");
   }

   if (!gaussian_app.CreateFence(gaussian_fence))
   {
      LogFatalError("Failed creating fence.");
   }

   if (!gaussian_app.SetInputBuffer(compute_pipeline, 0, input_image_buffer) ||
       !gaussian_app.SetInputBuffer(compute_pipeline, 1, input_gaussian_buffer) ||
       !gaussian_app.SetOutputBuffer(compute_pipeline, 2, output_image_buffer))
   {
      LogFatalError("Failed to setting input/output buffers");
   }

   if (!gaussian_app.UploadPipeline(compute_pipeline))
   {
      LogFatalError("Failed uploading pipeline");
   }

   VkCommandBuffer command_buffer;
   if (!gaussian_app.StartRecordingCommandBuffer("gaussian cmdbuf", command_buffer))
   {
      LogFatalError("Failed creating command buffer contents");
   }
   gaussian_app.GetComputeDevice()->SetObjectName(VK_OBJECT_TYPE_COMMAND_BUFFER,
                                                  reinterpret_cast<uint64_t>(command_buffer),
                                                  "Gaussian Command Buffer");
   if (!gaussian_app.BindPipeline(compute_pipeline, command_buffer))
   {
      LogFatalError("Failed binding pipeline");
   }

   PushConstants push_constant         = {};
   push_constant.data_vec.x            = image_width;
   push_constant.data_vec.y            = image_height;
   push_constant.data_vec.z            = image_comps;
   push_constant.gaussian_kernel_width = kernel_width;
   vkCmdPushConstants(command_buffer,
                      compute_pipeline->GetVkPipelineLayout(),
                      VK_SHADER_STAGE_COMPUTE_BIT,
                      0,
                      sizeof(PushConstants),
                      &push_constant);
   if (!gaussian_app.StopRecordingCommandBuffer(
         command_buffer, image_width / workgroup_size, image_height / workgroup_size, 1))
   {
      LogFatalError("Failed stopping command buffer");
   }
   if (!gaussian_app.SubmitCommandBuffer(
         command_buffer, VK_NULL_HANDLE, VK_NULL_HANDLE, gaussian_fence))
   {
      LogFatalError("Failed submitting command buffer contents");
   }
   if (!gaussian_app.WaitOnCommandBuffer(command_buffer, gaussian_fence))
   {
      LogFatalError("Failed waiting on command buffer");
   }
   gaussian_app.SaveImageFromBuffer(output_image_buffer,
                                    "gaussian.jpeg",
                                    image_width,
                                    image_height,
                                    buffer_comps,
                                    image_comps,
                                    sizeof(uint32_t));

   gaussian_app.UnloadPipeline(compute_pipeline);
   gaussian_app.FreeCommandBuffer(command_buffer);
   gaussian_app.FreePipeline(compute_pipeline);
   gaussian_app.DestroyFence(gaussian_fence);

   LogInfo("Succeeded to end!");
   return 0;
}
