//
// Copyright (c) Mark Young, 2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "vulkan_compute_app.hpp"

struct PushConstants
{
   struct
   {
      uint32_t x;
      uint32_t y;
   } image_dimensions;
   uint32_t max_iterations;
   float    time;
};

int
main(int argc, const char **argv)
{
   int32_t                image_width  = 4096;
   int32_t                image_height = 4096;
   int32_t                buffer_comps = 4;
   int32_t                image_comps  = 3;
   uint8_t                comp_size;
   const int32_t          workgroup_size = 16; // NOTE: This number MUST match the shader's
   VulkanComputeApp       julia_fractal_app("Julia Fractal App");
   VulkanComputeShader   *compute_shader;
   VulkanComputePipeline *compute_pipeline;

   VulkanComputeBuffer *output_image_buffer = julia_fractal_app.CreateBlankBuffer(
     "output_image",
     static_cast<uint64_t>(image_width * image_height * buffer_comps * sizeof(uint32_t)));

   compute_pipeline = julia_fractal_app.CreateComputePipeline(
     "main_pipeline", "julia_fractal.spv", &compute_shader, "main", sizeof(PushConstants));
   if (compute_pipeline == nullptr)
   {
      LogFatalError("Failed to create compute pipeline.");
   }

   if (!julia_fractal_app.SetOutputBuffer(compute_pipeline, 0, output_image_buffer))
   {
      LogFatalError("Failed to setting input/output buffers");
   }

   if (!julia_fractal_app.UploadPipeline(compute_pipeline))
   {
      LogFatalError("Failed uploading pipeline");
   }

   VkCommandBuffer command_buffer;
   if (!julia_fractal_app.StartRecordingCommandBuffer("julia fractal cmdbuf", command_buffer))
   {
      LogFatalError("Failed creating command buffer contents");
   }
   if (!julia_fractal_app.BindPipeline(compute_pipeline, command_buffer))
   {
      LogFatalError("Failed binding pipeline");
   }

   PushConstants push_constant      = {};
   push_constant.image_dimensions.x = image_width;
   push_constant.image_dimensions.y = image_height;
   push_constant.max_iterations     = 50;
   push_constant.time               = 75.0;
   vkCmdPushConstants(command_buffer,
                      compute_pipeline->GetVkPipelineLayout(),
                      VK_SHADER_STAGE_COMPUTE_BIT,
                      0,
                      sizeof(PushConstants),
                      &push_constant);

   output_image_buffer->TransitionFromOutputToInput(command_buffer);
   if (!julia_fractal_app.StopRecordingCommandBuffer(
         command_buffer, image_width / workgroup_size, image_height / workgroup_size, 1))
   {
      LogFatalError("Failed stopping command buffer");
   }
   if (!julia_fractal_app.SubmitCommandBuffer(command_buffer, nullptr, nullptr, VK_NULL_HANDLE))
   {
      LogFatalError("Failed submitting command buffer contents");
   }
   if (!julia_fractal_app.WaitOnCommandBuffer(command_buffer, VK_NULL_HANDLE))
   {
      LogFatalError("Failed waiting on command buffer");
   }
   julia_fractal_app.SaveImageFromBuffer(output_image_buffer,
                                         "julia_fractal.jpeg",
                                         image_width,
                                         image_height,
                                         buffer_comps,
                                         image_comps,
                                         sizeof(uint32_t));

   julia_fractal_app.UnloadPipeline(compute_pipeline);
   julia_fractal_app.FreeCommandBuffer(command_buffer);
   julia_fractal_app.FreePipeline(compute_pipeline);

   LogInfo("Succeeded to end!");
   return 0;
}
