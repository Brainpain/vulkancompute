# Julia Fractal Sample

This sample uses push constants to define the image size and number of iterations
and Vulkan compute to generate a Julia Set Fractal output image output through
a storage buffer.

For fun, feel free to modify the "time" variable in the source file to change
the resulting image.

### Uses
 - Push constants
 - Storage buffer for output image

## Resources

### Shader

The shader `julia_fractal.spv` is generated at compile time from the GLSL shader file
[julia_fractal.glsl](../../resources/shaders/julia.glsl) using Glslang.

### Source

The source file [julia_fractal.cpp](./julia_fractal.cpp) is found in the current directory.

## Gaussian Expected Output

**Console:**

```
VulkanCompute [INFO] - InitAndSelectDevice: DebugUtilsPresent
VulkanCompute [INFO] - Input image loaded
VulkanCompute [INFO] - Submitting command buffer
VulkanCompute [INFO] - Output image saved
VulkanCompute [INFO] - Succeeded to end!
```

**Generate Images:**

A new image called `julia_fractal.jpeg` is created by running the Julia fractal
shader program and is generated in the current directory.

