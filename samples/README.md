# Samples

---

Samples are built into the user-defined BUILD folder under the "bin" sub-folder.
The "bin" sub-folder contains all executables and the corresponding resource files.

Provided Samples:

| Sample | Description | Example Output Image |
|-----|---------------------|----|
| [Data Copy](./data_copy/README.md)     | Example performing a simple copy of data from one buffer to another. | N/A |
| [Julia Set Fractal](./julia_fractal/README.md) | Example using compute shaders to create a static image using a Julia Set Fractal. This expands by using push constants to define the size of the image. | ![Julia Set](./readme_images/julia_fractal.jpeg)
| [Edge Detect](./edge_detect/README.md) | Example using compute shaders to perform edge detection on an image. This example uses push constants as well as an input image that is processed to produce a new output image containing only the detected edges. | ![Edge Detect](./readme_images/edge_detect.jpeg)
| [Gaussian Blur](./gaussian/README.md) | Example using compute shaders to perform a Gaussian blur on an image.  This example uses push constants, an input image, and an input storage buffer containing the gaussian kernel. | ![Gaussian](./readme_images/gaussian.jpeg)
| [Two Compute Programs](./two_compute_programs/README.md) | Example using two different compute shaders in a single submission.  The first compute program performs a Gaussian blur on an image.  The second compute program performs a diff of the blurred image versus the original image.  This example uses push constants, an input image, an output image that is then used by the next pass as an input image, and an input storage buffer containing the gaussian kernel. | ![Two Compute Programs](./readme_images/two_compute_programs.jpeg)

