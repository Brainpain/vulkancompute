# Two Compute Programs Sample

Example using two different compute shaders in a single submission.
The first compute program performs a Gaussian blur on an image.
The second compute program performs a diff of the blurred image versus the
original image.

### Uses
 - Push constants
 - Storage buffer for input image
 - Storage buffer for gaussian kernel as an array
 - Storage buffer for output and then input image

## Resources

### Shaders

 - gaussian.spv
   - Generated from [gaussian.glsl](../../resources/shaders/gaussian.glsl)
 - diff_image.spv
   - Generated from [diff_image.glsl](../../resources/shaders/diff_image.glsl)

### Source

The source file [two_compute_programs.cpp](./two_compute_programs.cpp) is found
in the current directory.

### Input Image

* [miata_rf.jpeg](../../resources/images/miata_rf.jpeg)

## Expected Output

**Console:**

```
VulkanCompute [INFO] - InitAndSelectDevice: DebugUtilsPresent
VulkanCompute [INFO] - InitAndSelectDevice: Created device with Vulkan API version 1.3
VulkanCompute [INFO] - Input image loaded
VulkanCompute [INFO] - Submitting command buffer
VulkanCompute [INFO] - Submitting command buffer
VulkanCompute [INFO] - Output image two_compute_programs.jpeg saved
VulkanCompute [INFO] - Succeeded to end!
```

**Generate Images:**

A new images called `two_compute_programs.jpeg` is generated in the current
directory.

