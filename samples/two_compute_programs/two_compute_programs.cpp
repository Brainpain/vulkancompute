//
// Copyright (c) Mark Young, 2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "utility.hpp"
#include "vulkan_compute_app.hpp"

struct PushConstants
{
   struct
   {
      uint32_t x;
      uint32_t y;
      uint32_t z;
      uint32_t w;
   } data_vec;
   int32_t gaussian_kernel_width;
};

int
main(int argc, const char **argv)
{
   int32_t                image_width;
   int32_t                image_height;
   int32_t                image_comps;
   int32_t                buffer_comps = 4;
   uint8_t                comp_size;
   int32_t                kernel_width = 11;
   float                  kernel_sigma = float(kernel_width / 7);
   std::vector<float>     gaussian_kernel;
   const int32_t          workgroup_size = 16; // NOTE: This number MUST match the shader's
   VulkanComputeApp       two_pass_app("Two Compute Passes Application");
   VulkanComputeShader   *gaussian_shader;
   VulkanComputeShader   *diff_image_shader;
   VkFence                completion_fence   = VK_NULL_HANDLE;
   VkSemaphore            gaussian_semaphore = VK_NULL_HANDLE;
   VulkanComputePipeline *gaussian_pipeline;
   VulkanComputePipeline *diff_pipeline;

   if (kernel_sigma < 1.f)
   {
      kernel_sigma = 1.f;
   }

   if (!vulkan_compute ::utility::GenerateGaussianKernel(
         kernel_width, kernel_sigma, gaussian_kernel))
   {
      LogFatalError("Failed to create gaussian kernel.");
   }

   VulkanComputeBuffer *input_image_buffer = two_pass_app.LoadImageIntoBuffer(
     "miata_rf.jpeg", image_width, image_height, image_comps, comp_size);
   size_t               gaussian_kernel_size = gaussian_kernel.size() * sizeof(float);
   VulkanComputeBuffer *gaussian_kernel_buffer =
     two_pass_app.CreateBlankBuffer("gaussian_kernel", gaussian_kernel_size);
   int32_t *input_values =
     reinterpret_cast<int32_t *>(gaussian_kernel_buffer->MapMemoryPointer(gaussian_kernel_size, 0));
   memcpy(input_values, gaussian_kernel.data(), gaussian_kernel_size);
   gaussian_kernel_buffer->UnmapMemoryPointer();

   VulkanComputeBuffer *gaussian_image_buffer = two_pass_app.CreateBlankBuffer(
     "gaussian_image",
     static_cast<uint64_t>(image_width * image_height * buffer_comps * sizeof(uint32_t)));
   VulkanComputeBuffer *output_image_buffer = two_pass_app.CreateBlankBuffer(
     "output_image",
     static_cast<uint64_t>(image_width * image_height * buffer_comps * sizeof(uint32_t)));

   gaussian_pipeline = two_pass_app.CreateComputePipeline(
     "main_pipeline", "gaussian.spv", &gaussian_shader, "main", sizeof(PushConstants));
   if (gaussian_pipeline == nullptr)
   {
      LogFatalError("Failed to create compute pipeline.");
   }

   if (!two_pass_app.SetInputBuffer(gaussian_pipeline, 0, input_image_buffer) ||
       !two_pass_app.SetInputBuffer(gaussian_pipeline, 1, gaussian_kernel_buffer) ||
       !two_pass_app.SetOutputBuffer(gaussian_pipeline, 2, gaussian_image_buffer))
   {
      LogFatalError("Failed to setting input/output buffers");
   }

   if (!two_pass_app.CreateFence(completion_fence))
   {
      LogFatalError("Failed to create fence");
   }

   if (!two_pass_app.CreateSemaphore(gaussian_semaphore))
   {
      LogFatalError("Failed to create semaphore");
   }

   if (!two_pass_app.UploadPipeline(gaussian_pipeline))
   {
      LogFatalError("Failed uploading guassian pipeline");
   }

   VkCommandBuffer gaussian_command_buffer;
   if (!two_pass_app.StartRecordingCommandBuffer("gaussian cmdbuf", gaussian_command_buffer))
   {
      LogFatalError("Failed creating gaussian command buffer contents");
   }
   if (!two_pass_app.BindPipeline(gaussian_pipeline, gaussian_command_buffer))
   {
      LogFatalError("Failed binding gaussian pipeline");
   }
   PushConstants push_constant         = {};
   push_constant.data_vec.x            = image_width;
   push_constant.data_vec.y            = image_height;
   push_constant.data_vec.z            = image_comps;
   push_constant.gaussian_kernel_width = kernel_width;
   vkCmdPushConstants(gaussian_command_buffer,
                      gaussian_pipeline->GetVkPipelineLayout(),
                      VK_SHADER_STAGE_COMPUTE_BIT,
                      0,
                      sizeof(PushConstants),
                      &push_constant);
   if (!two_pass_app.StopRecordingCommandBuffer(
         gaussian_command_buffer, image_width / workgroup_size, image_height / workgroup_size, 1))
   {
      LogFatalError("Failed stopping gaussian command buffer");
   }
   if (!two_pass_app.SubmitCommandBuffer(
         gaussian_command_buffer, nullptr, &gaussian_semaphore, VK_NULL_HANDLE))
   {
      LogFatalError("Failed submitting gaussian command buffer contents");
   }

   diff_pipeline = two_pass_app.CreateComputePipeline(
     "main_pipeline", "diff_image.spv", &diff_image_shader, "main", sizeof(PushConstants));
   if (diff_pipeline == nullptr)
   {
      LogFatalError("Failed to create compute pipeline.");
   }

   if (!two_pass_app.SetInputBuffer(diff_pipeline, 0, input_image_buffer) ||
       !two_pass_app.SetInputBuffer(diff_pipeline, 1, gaussian_image_buffer) ||
       !two_pass_app.SetOutputBuffer(diff_pipeline, 2, output_image_buffer))
   {
      LogFatalError("Failed to setting input/output buffers");
   }

   if (!two_pass_app.UploadPipeline(diff_pipeline))
   {
      LogFatalError("Failed uploading diff pipeline");
   }

   VkCommandBuffer diff_command_buffer;
   if (!two_pass_app.StartRecordingCommandBuffer("diff cmdbuf", diff_command_buffer))
   {
      LogFatalError("Failed creating diff command buffer contents");
   }
   gaussian_image_buffer->TransitionFromOutputToInput(diff_command_buffer);
   if (!two_pass_app.BindPipeline(diff_pipeline, diff_command_buffer))
   {
      LogFatalError("Failed binding diff pipeline");
   }
   vkCmdPushConstants(diff_command_buffer,
                      diff_pipeline->GetVkPipelineLayout(),
                      VK_SHADER_STAGE_COMPUTE_BIT,
                      0,
                      sizeof(PushConstants),
                      &push_constant);
   if (!two_pass_app.StopRecordingCommandBuffer(
         diff_command_buffer, image_width / workgroup_size, image_height / workgroup_size, 1))
   {
      LogFatalError("Failed stopping diff command buffer");
   }
   if (!two_pass_app.SubmitCommandBuffer(
         diff_command_buffer, &gaussian_semaphore, nullptr, completion_fence))
   {
      LogFatalError("Failed submitting diff command buffer contents");
   }
   if (!two_pass_app.WaitOnCommandBuffer(diff_command_buffer, completion_fence))
   {
      LogFatalError("Failed waiting on command buffer");
   }
   two_pass_app.SaveImageFromBuffer(output_image_buffer,
                                    "two_compute_programs.jpeg",
                                    image_width,
                                    image_height,
                                    buffer_comps,
                                    image_comps,
                                    sizeof(uint32_t));

   two_pass_app.DestroySemaphore(gaussian_semaphore);
   two_pass_app.DestroyFence(completion_fence);

   two_pass_app.UnloadPipeline(gaussian_pipeline);
   two_pass_app.UnloadPipeline(diff_pipeline);
   two_pass_app.FreeCommandBuffer(gaussian_command_buffer);
   two_pass_app.FreeCommandBuffer(diff_command_buffer);
   two_pass_app.FreePipeline(gaussian_pipeline);
   two_pass_app.FreePipeline(diff_pipeline);

   LogInfo("Succeeded to end!");
   return 0;
}
