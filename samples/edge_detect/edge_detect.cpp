//
// Copyright (c) Mark Young, 2020,2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include "vulkan_compute_app.hpp"

struct PushConstants
{
   struct
   {
      uint32_t x;
      uint32_t y;
      uint32_t z;
      uint32_t w;
   } data_vec;
};

int
main(int argc, const char **argv)
{
   int32_t                image_width;
   int32_t                image_height;
   int32_t                image_comps;
   int32_t                buffer_comps = 4;
   uint8_t                comp_size;
   const int32_t          workgroup_size = 16; // NOTE: This number MUST match the shader's
   VulkanComputeApp       edge_detect_app("Edge Detect Sample");
   VulkanComputeShader   *compute_shader;
   VulkanComputePipeline *compute_pipeline;

   VulkanComputeBuffer *input_image_buffer = edge_detect_app.LoadImageIntoBuffer(
     "miata_rf.jpeg", image_width, image_height, image_comps, comp_size);
   VulkanComputeBuffer *output_image_buffer = edge_detect_app.CreateBlankBuffer(
     "output_image",
     static_cast<uint64_t>(image_width * image_height * buffer_comps * sizeof(uint32_t)));

   compute_pipeline = edge_detect_app.CreateComputePipeline(
     "main_pipeline", "edge_detect.spv", &compute_shader, "main", sizeof(PushConstants));
   if (compute_pipeline == nullptr)
   {
      LogFatalError("Failed to create compute pipeline.");
   }

   if (!edge_detect_app.SetInputBuffer(compute_pipeline, 0, input_image_buffer) ||
       !edge_detect_app.SetOutputBuffer(compute_pipeline, 1, output_image_buffer))
   {
      LogFatalError("Failed to setting input/output buffers");
   }

   if (!edge_detect_app.UploadPipeline(compute_pipeline))
   {
      LogFatalError("Failed uploading pipeline");
   }

   VkCommandBuffer command_buffer;
   if (!edge_detect_app.StartRecordingCommandBuffer("edge detect cmdbuf", command_buffer))
   {
      LogFatalError("Failed creating command buffer contents");
   }
   if (!edge_detect_app.BindPipeline(compute_pipeline, command_buffer))
   {
      LogFatalError("Failed binding pipeline");
   }
   PushConstants push_constant = {};
   push_constant.data_vec.x    = image_width;
   push_constant.data_vec.y    = image_height;
   push_constant.data_vec.z    = image_comps;
   vkCmdPushConstants(command_buffer,
                      compute_pipeline->GetVkPipelineLayout(),
                      VK_SHADER_STAGE_COMPUTE_BIT,
                      0,
                      sizeof(PushConstants),
                      &push_constant);
   if (!edge_detect_app.StopRecordingCommandBuffer(
         command_buffer, image_width / workgroup_size, image_height / workgroup_size, 1))
   {
      LogFatalError("Failed stopping command buffer");
   }
   if (!edge_detect_app.SubmitCommandBuffer(command_buffer, nullptr, nullptr, VK_NULL_HANDLE))
   {
      LogFatalError("Failed submitting command buffer contents");
   }
   if (!edge_detect_app.WaitOnCommandBuffer(command_buffer, VK_NULL_HANDLE))
   {
      LogFatalError("Failed waiting on command buffer");
   }

   edge_detect_app.SaveImageFromBuffer(output_image_buffer,
                                       "edge_detect.jpeg",
                                       image_width,
                                       image_height,
                                       buffer_comps,
                                       image_comps,
                                       sizeof(uint32_t));

   edge_detect_app.UnloadPipeline(compute_pipeline);
   edge_detect_app.FreeCommandBuffer(command_buffer);
   edge_detect_app.FreePipeline(compute_pipeline);

   LogInfo("Succeeded to end!");
   return 0;
}
