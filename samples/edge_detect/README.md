# Edge Detect Sample

Simple compute edge detection program which takes an input storage image
and runs an edge detection compute shader to generate an output storage image.

### Uses
 - Push constants
 - Storage buffer for input image
 - Storage buffer for output image

## Resources

### Shader

The shader edge_detect.spv is generated at compile time from the GLSL shader file
[edge_detect.glsl](../../resources/shaders/edge_detect.glsl) using Glslang.

### Source

The source file [edge_detect.cpp](./edge_detect.cpp) is found in the current directory.

### Input Image

* [us_and_dogs.jpeg](../../resources/images/us_and_dogs.jpeg)

## Edge_Detect Expected Output

**Console:**

```
VulkanCompute [INFO] - InitAndSelectDevice: DebugUtilsPresent
VulkanCompute [INFO] - Input image loaded
VulkanCompute [INFO] - Submitting command buffer
VulkanCompute [INFO] - Output image saved
VulkanCompute [INFO] - Succeeded to end!
```

**Generate Images:**

A new images called `edge_detect.jpeg` is generated in the current directory.

