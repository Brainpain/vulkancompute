# Build Instructions

Instructions for building this repository on Linux, and Windows.

## Index

1. [Contributing](#contributing-to-the-repository)
2. [Repository Set-Up](#repository-set-up)
3. [Windows Build](#building-on-windows)
4. [Linux Build](#building-on-linux)

## Contributing to the Repository

If you intend to contribute, the preferred work flow is for you to develop
your contribution in a fork of this repository in your GitLab account and
then submit a pull request.
Please see the [CONTRIBUTING.md](CONTRIBUTING.md) file in this repository for more details.

## Repository Set-Up

### Pre-Requisites

#### Tools / Utilities

1. CMake >= 3.17.2
2. C++ >= c++17 compiler. See platform-specific sections below for supported compiler versions.
3. Python >= 3.8
4. Git

#### Update Your Graphics Display Drivers
This repository does not contain a Vulkan-capable driver.
Before proceeding, it is strongly recommended that you obtain a Vulkan driver from your
graphics hardware vendor and install it properly.

#### Validation layers

Because Validation layers require a little bit more work, I don't currently include them
as a submodule.  If you are going to build a debug version of this project, it requires
that Validation Layers are present.

For that reason, simply download the Vulkan SDK from the LunarXchange web-site:
https://vulkan.lunarg.com/

## Building On Windows

### Windows Build Requirements

### Windows Build - Microsoft Visual Studio

1. Open a Developer Command Prompt for VS201x
2. Change directory to the root of the cloned git repository
3. Create a `build` directory: `mkdir build`
4. Change into that directory: `cd build`
5. Update the dependencies `python3 ..\scripts\update_deps.py`
6. Run Cmake

For example, assuming an SDK is installed, for VS2017 (generators for other
versions are [specified here](#cmake-visual-studio-generators)):

```
cmake "Visual Studio 15 2017 Win64" ..
```

This will create a Windows solution file named `VulkanCompute.sln` in the build
directory.

If no SDK is installed, you will need to point to the appropriate Vulkan Headers
and Vulkan Loader repository.


Launch Visual Studio and open the "VulkanCompute.sln" solution file in the build folder.
You may select "Debug" or "Release" from the Solution Configurations drop-down list.
Start a build by selecting the Build->Build Solution menu item.
This solution copies the loader it built to each program's build directory
to ensure that the program uses the loader built from this solution.

## Building On Linux

### Linux Build Requirements

This repository has been built and tested on the two most recent Ubuntu LTS versions.
Currently, the oldest supported version is Ubuntu 14.04, meaning that the minimum supported compiler versions are GCC 4.8.2 and Clang 3.4, although earlier versions may work.
It should be straightforward to adapt this repository to other Linux distributions.

**Required Package List:**

    sudo apt-get install git cmake build-essential python3

### Linux Build

Example debug build

1. In a Linux terminal, change into the root of the cloned git repository
2. Create a `build` directory: `mkdir build`
3. Change into that directory: `cd build`
4. Update the dependencies `python3 ../scripts/update_deps.py`
5. Change back to the top directory: `cd ..`
5. Run Cmake to generate the build files: `cmake -S . -B build -C build/helper.cmake -DCMAKE_BUILD_TYPE=DEBUG`
4. Run cmake to build: `cmake --build build`
