//
// Copyright (c) Mark Young, 2023, All rights reserved.
// SPDX-License-Identifier: MIT
//

#include <cstdint>
#include <cstring>
#include <vector>

namespace vulkan_compute
{
namespace utility
{

bool
GenerateGaussianKernel(uint32_t width, float sigma, std::vector<float> &gaussian_kernel);

} // namespace utility
} // namespace vulkan_compute