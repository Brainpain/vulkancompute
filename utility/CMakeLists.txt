#
# Copyright (c) Mark Young, 2018-2020, All rights reserved.
# SPDX-License-Identifier: MIT
#

add_library(utility "")

target_sources(utility
               PRIVATE
                   utility.hpp
                   utility.cpp
              )

target_include_directories(utility
                           PUBLIC
                               ${CMAKE_CURRENT_SOURCE_DIR}
                          )

target_compile_options(utility
                        PUBLIC
                           -std=c++11
                      )

if (${GLOBE_WINDOWS_ENABLED})

    target_compile_definitions(utility
                                PUBLIC
                                    -DWIN32
                                    -D_WIN32
                                    -DWIN32_LEAN_AND_MEAN
                                    -D_CRT_SECURE_NO_WARNINGS
                                    -D_USE_MATH_DEFINES
                                )


endif()
