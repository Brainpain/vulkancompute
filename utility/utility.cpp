#

#include "utility.hpp"

#include <cmath>
#include <iomanip>
#include <iostream>

namespace vulkan_compute
{
namespace utility
{

bool
GenerateGaussianKernel(uint32_t width, float sigma, std::vector<float> &gaussian_kernel)
{
   if (width % 2 == 0)
   {
      return false;
   }
   gaussian_kernel.resize(width * width);

   // Gaussian Kernel is:
   //                                 x^2 + y^2
   //  G(x,y) =        1         - ---------------
   //           -------------- e^   2 * sigma^2
   //            2*pi*sigma^2
   //
   int32_t half_width         = (width - 1) / 2;
   float   s                  = 2.0f * sigma * sigma;
   float   one_over_s_pi      = 1.f / (s * M_PI);
   float   kernel_value_total = 0.f;

   for (int32_t x = 0; x < width; ++x)
   {
      for (int32_t y = 0; y < width; ++y)
      {
         float x_value = powf((x - half_width), 2.f);
         float y_value = powf((y - half_width), 2.f);
         float kernel_value = exp(-((x_value + y_value) / s)) * one_over_s_pi;
         gaussian_kernel[(y * width) + x] = kernel_value;
         kernel_value_total += kernel_value;
      }
   }

   // Normalize the kernel values
   for (int32_t x = 0; x < width; ++x)
   {
      for (int32_t y = 0; y < width; ++y)
      {
         gaussian_kernel[y * width + x] /= kernel_value_total;
      }
   }
   return true;
}

} // namespace utility
} // namespace vulkan_compute