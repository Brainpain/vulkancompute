# VulkanCompute

VulkanCompute is a personal development tree that is a work in progress.
I intend this to just be a place I can experiment in my personal time
with compute on Vulkan.
Most of the core functionality with creating a device and uploading data
buffers is hidden in the "base" library.
My intent with that is to keep the basic behavior to the samples and
that way if anyone ever uses it as a refeernce they can delve into the
particular sections they care about.

## License

This project is licensed under the MIT License.
More information about this license can be read in the provided
[LICENSE.txt](LICENSE.txt) file.
Feel free to use it as you see fit!

Software pulled in from external repositories using the `update_deps.py` script,
however, are licensed based on their own terms.
Please refer to their individual license files for further information.

## Building

Instructions for building this repository can be found in the
[Build.md](./BUILD.md) file.

## Samples

Samples are built into the user-defined BUILD folder under the "bin" sub-folder.
The "bin" sub-folder contains all executables and the corresponding resource
files.
Again, they all use the "base" library files to do most of the heavy lifting.

More information can be found in the [Samples](./samples/README.md) file.
